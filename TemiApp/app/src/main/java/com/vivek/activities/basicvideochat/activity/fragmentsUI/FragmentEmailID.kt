package com.vivek.activities.basicvideochat.activity.fragmentsUI

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.activity.ActivityPatientDetailsUI
import com.vivek.activities.basicvideochat.model.*
import com.vivek.activities.basicvideochat.network.OpenTokConfig
import com.vivek.activities.databinding.FragmentEmailIDBinding

/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class FragmentEmailID : androidx.fragment.app.Fragment() {
    private val hideHandler = Handler()
    var patientEmailEditText: EditText? = null
    lateinit var imageButtonSendEmail: Button
    lateinit var imageButtonSendBackToHome: Button
    var TAG: String = "inside email fragment"

    @Suppress("InlinedApi")
    private val hidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        val flags =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        activity?.window?.decorView?.systemUiVisibility = flags
        (activity as? AppCompatActivity)?.supportActionBar?.hide()
    }
    private val showPart2Runnable = Runnable {
        // Delayed display of UI elements
        fullscreenContentControls?.visibility = View.VISIBLE
    }
    private var visible: Boolean = false
    private val hideRunnable = Runnable {
//        hide()
    }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */

    private var dummyButton: Button? = null
    private var fullscreenContent: View? = null
    private var fullscreenContentControls: View? = null

    private var _binding: FragmentEmailIDBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEmailIDBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI(_binding!!.root.findViewById(R.id.emailScreen))
        visible = true

        patientEmailEditText =
            _binding!!.root.findViewById<View>(R.id.email_edit_text_input_edit_text) as EditText
        patientEmailEditText!!.addTextChangedListener(vitalsText)
        imageButtonSendEmail = _binding!!.root.findViewById<View>(R.id.sendPrescription) as Button
        imageButtonSendEmail.setOnClickListener {
            (activity as ActivityPatientDetailsUI).sendPatientEmail()
            if ((activity as ActivityPatientDetailsUI).emailSendStatus!="ERROR") {
                beginAgainForNewUser()
            }
        }
        imageButtonSendBackToHome = _binding!!.root.findViewById<View>(R.id.backToHome) as Button
        imageButtonSendBackToHome.setOnClickListener {
            beginAgainForNewUser()
        }
    }

    private val vitalsText: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            imageButtonSendEmail.isEnabled = patientEmailEditText?.text!!.isNotEmpty()
            imageButtonSendEmail.isActivated = imageButtonSendEmail.isEnabled

            if (patientEmailEditText!!.text!!.isNotEmpty()) {
                if (Patterns.EMAIL_ADDRESS.matcher(patientEmailEditText!!.text!!).matches()) {
                    imageButtonSendEmail.isEnabled = true
                    imageButtonSendEmail.isActivated = true
                    (activity as ActivityPatientDetailsUI).patientEmail?.sessionId =
                        OpenTokConfig.SESSION_ID
                    (activity as ActivityPatientDetailsUI).patientEmail?.doctor_id =
                        (activity as ActivityPatientDetailsUI).userData.getSessionBodyParam.doctorName
                    (activity as ActivityPatientDetailsUI).patientEmail?.email =
                        patientEmailEditText!!.text.toString()
                    Log.i("email", "correct")
                } else {
                    imageButtonSendEmail.isEnabled = false
                    imageButtonSendEmail.isActivated = false
                    Log.i("email", "incorrect")
                }
            }

            if (imageButtonSendEmail.isEnabled && patientEmailEditText!!.text!!.isNotEmpty()) {
                imageButtonSendEmail.alpha = 1.0F
            } else {
                imageButtonSendEmail.alpha = 0.5F
            }
        }

        override fun afterTextChanged(s: Editable) {
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    fun setupUI(view: View) {
        // Set up touch listener for non-text box views to hide keyboard.
//        view !is EditText &&
        if (view !is LinearLayout) {
            view.setOnTouchListener { _, _ ->
                hideSoftKeyboard(activity as ActivityPatientDetailsUI)
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                setupUI(innerView)
            }
        }
    }

    private fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager: InputMethodManager = activity.getSystemService(
            AppCompatActivity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(
                activity.currentFocus!!.windowToken,
                0
            )
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }

    override fun onPause() {
        super.onPause()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // Clear the systemUiVisibility flag
        activity?.window?.decorView?.systemUiVisibility = 0
//        show()
    }

    override fun onDestroy() {
        super.onDestroy()
        dummyButton = null
        fullscreenContent = null
        fullscreenContentControls = null
    }
//
//    private fun toggle() {
//        if (visible) {
//            hide()
//        } else {
//            show()
//        }
//    }
//
//    private fun hide() {
//        // Hide UI first
//        fullscreenContentControls?.visibility = View.GONE
//        visible = false
//
//        // Schedule a runnable to remove the status and navigation bar after a delay
//        hideHandler.removeCallbacks(showPart2Runnable)
//        hideHandler.postDelayed(hidePart2Runnable, UI_ANIMATION_DELAY.toLong())
//    }
//
//    @Suppress("InlinedApi")
//    private fun show() {
//        // Show the system bar
//        fullscreenContent?.systemUiVisibility =
//            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
//                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//        visible = true
//
//        // Schedule a runnable to display UI elements after a delay
//        hideHandler.removeCallbacks(hidePart2Runnable)
//        hideHandler.postDelayed(showPart2Runnable, UI_ANIMATION_DELAY.toLong())
//        (activity as? AppCompatActivity)?.supportActionBar?.show()
//    }
//
//    /**
//     * Schedules a call to hide() in [delayMillis], canceling any
//     * previously scheduled calls.
//     */
//    private fun delayedHide(delayMillis: Int) {
//        hideHandler.removeCallbacks(hideRunnable)
//        hideHandler.postDelayed(hideRunnable, delayMillis.toLong())
//    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private const val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private const val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private const val UI_ANIMATION_DELAY = 300
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun beginAgainForNewUser() {
        Log.i(TAG, "inside beginAgainForNewUser")
        setInitialValues()
        val fragment: FragmentWelcomeScreen = FragmentWelcomeScreen()
        val fragmentManager = requireActivity().supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.tempFragment, fragment, "SecondWeightScreen")
        fragmentTransaction.addToBackStack("SecondWeightScreen")
        fragmentTransaction.commit()
    }

    private fun setInitialValues() {
        Log.i(TAG, "inside setInitialValues")
        ActivityPatientDetailsUI.callCompleted = false
        ActivityPatientDetailsUI.callCompletedNumber = 0
        (activity as ActivityPatientDetailsUI).dataEditingComplete = false
        //Resetting userData of ActivityPatientDetailsUI
        (activity as ActivityPatientDetailsUI).userData = UserData("")
        (activity as ActivityPatientDetailsUI).userData.bloodPressure = BloodPressure("", "")
        (activity as ActivityPatientDetailsUI).userData.vitals =
            Vitals((activity as ActivityPatientDetailsUI).userData.bloodPressure, "", "", "")
        (activity as ActivityPatientDetailsUI).userData.personal_details =
            PersonalDetails("", "", "")
        (activity as ActivityPatientDetailsUI).userData.getSessionBodyParam = GetSessionBodyParam(
            (activity as ActivityPatientDetailsUI).userData.personal_details,
            (activity as ActivityPatientDetailsUI).userData.vitals,
            (activity as ActivityPatientDetailsUI).userData.doctorName
        )
        (activity as ActivityPatientDetailsUI).patientEmailResponse.setMsg("")
        (activity as ActivityPatientDetailsUI).patientEmailResponse.setData(Data(""))
        (activity as ActivityPatientDetailsUI).patientEmail = EmailRequest("", "", "")
    }
}