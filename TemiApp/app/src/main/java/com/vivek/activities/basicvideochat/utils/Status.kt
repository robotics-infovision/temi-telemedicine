package com.vivek.activities.basicvideochat.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}