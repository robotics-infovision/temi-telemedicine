package com.vivek.activities.basicvideochat.model

import java.io.Serializable

data class PersonalDetails(
    var name: String = "",
    var age: String = "",
    var gender: String= "",
) : Serializable