package com.vivek.activities.basicvideochat.activity.fragmentsUI

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import com.aigestudio.wheelpicker.WheelPicker
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.activity.ActivityPatientDetailsUI
import com.vivek.activities.basicvideochat.activity.ActivityVideoCall
import com.vivek.activities.basicvideochat.model.*
import com.vivek.activities.databinding.FragmentAgeScreenBinding

/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class FragmentAgeScreen : Fragment(), WheelPicker.OnItemSelectedListener, View.OnClickListener {
    private val hideHandler = Handler()

    private var wheelLeft: WheelPicker? = null
    val bp1: MutableList<Int> = mutableListOf()
    var databp1: List<Int>? = null
    var imageButtonNext: ImageButton? = null
    var imageButtonBack: ImageButton? = null
    private var gotoBtnItemIndex: Int? = null
    var TAG: String = "AgeScreen"
    var ageScreenTag: String = "Age"
    var summaryButton: Button? = null
    var consultButton: Button? = null

    private var visible: Boolean = false

    private var fullscreenContent: View? = null
    private var fullscreenContentControls: View? = null

    private var _binding: FragmentAgeScreenBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAgeScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        visible = true
        allFunctionsOnCreated()
    }

    private fun allFunctionsOnCreated() {
        setVisiblityConsultationButton()
        setFindViewById()
        setCustomizedData()
        setOnClickListeners()
    }

    private fun setVisiblityConsultationButton() {
        if (!(activity as ActivityPatientDetailsUI).dataEditingComplete) {
            (_binding!!.root.findViewById<View>(R.id.consultButton) as Button).visibility =
                View.GONE
            (_binding!!.root.findViewById<View>(R.id.backToSummary) as Button).visibility =
                View.GONE
        } else {
            (_binding!!.root.findViewById<View>(R.id.nextButton) as ImageButton).visibility =
                View.GONE
            consultButton = _binding!!.root.findViewById<View>(R.id.consultButton) as Button
            summaryButton = _binding!!.root.findViewById<View>(R.id.backToSummary) as Button
            consultButton?.setOnClickListener {
                    establishVideoConference()
            }
            summaryButton?.setOnClickListener {
                callNextFragment()
            }
        }
    }

    private fun setFindViewById() {
        wheelLeft = _binding!!.root.findViewById<View>(R.id.main_wheel_left) as WheelPicker
        imageButtonNext = _binding!!.root.findViewById<View>(R.id.nextButton) as ImageButton?
        imageButtonBack = _binding!!.root.findViewById<View>(R.id.backButton) as ImageButton?
    }

    fun setOnClickListeners() {
        wheelLeft?.setOnItemSelectedListener(this)
        imageButtonNext?.setOnClickListener {
            if (imageButtonNext?.isActivated == true &&
                imageButtonNext?.isEnabled == true
            ) {
                Log.i(TAG, "next button clicked")
                callNextFragment()
            }
        }
        imageButtonBack?.setOnClickListener {
            Log.i(TAG, "back button clicked")
            callPreviousFragment()
        }
        if ((activity as ActivityPatientDetailsUI).dataEditingComplete) {
            setCurrentState()
        }
    }

    private fun setCustomizedData() {
        for (i in 1..150) {
            bp1.add(i - 1, i)
        }
        databp1 = bp1
        wheelLeft?.data = databp1
        if ((activity as ActivityPatientDetailsUI).userData.personal_details.gender == "MALE") {
            wheelLeft?.selectedItemPosition = 37
            wheelLeft?.setSelectedItemPosition(37, true)
            Log.i("ageScreen", "MALE Selected and wheelLeft ${wheelLeft?.selectedItemPosition}")
        } else {
            wheelLeft?.selectedItemPosition = 39
            wheelLeft?.setSelectedItemPosition(39, true)
            Log.i("ageScreen", "FEMALE Selected and wheelLeft ${wheelLeft?.selectedItemPosition}")
        }
        imageButtonNext?.setImageDrawable(((context?.let {
            androidx.core.content.ContextCompat.getDrawable(
                it,
                R.drawable.icon_next_disable
            )
        })))
        imageButtonNext?.isActivated = false
        imageButtonNext?.isEnabled = false
    }

    private fun setCurrentState() {
        wheelLeft?.selectedItemPosition =
            Integer.parseInt((activity as ActivityPatientDetailsUI).userData.personal_details.age) - 1
        wheelLeft?.setSelectedItemPosition(
            Integer.parseInt((activity as ActivityPatientDetailsUI).userData.personal_details.age) - 1,
            true
        )
    }

    override fun onResume() {
        super.onResume()
        allFunctionsOnCreated()
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        Log.i(
            "on Resume from age",
            (activity as ActivityPatientDetailsUI).userData.personal_details.age
        )
    }

    override fun onPause() {
        super.onPause()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        // Clear the systemUiVisibility flag
        activity?.window?.decorView?.systemUiVisibility = 0
    }

    override fun onDestroy() {
        super.onDestroy()
        fullscreenContent = null
        fullscreenContentControls = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemSelected(picker: WheelPicker, data: Any, position: Int) {
        var text = ""
        when (picker.id) {
            R.id.main_wheel_left -> {
                text = "Left:"
            }
        }
        (activity as ActivityPatientDetailsUI).userData.personal_details.age = data.toString()
        imageButtonNext?.setImageDrawable(((context?.let {
            androidx.core.content.ContextCompat.getDrawable(
                it,
                R.drawable.icon_next_enable
            )
        })))
        imageButtonNext?.isActivated = true
        imageButtonNext?.isEnabled = true
        consultButton?.isActivated = true
        consultButton?.isEnabled = true
        Log.i(TAG, "Age: ${(activity as ActivityPatientDetailsUI).userData.personal_details.age}")
    }

    override fun onClick(v: View?) {
//        wheelLeft?.selectedItemPosition = 40
//        randomlySetGotoBtnIndex()
    }

    private fun callNextFragment() {
        if (!(activity as ActivityPatientDetailsUI).dataEditingComplete) {
            val fragment: FragmentWeightScreen = FragmentWeightScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.tempFragment, fragment, ageScreenTag)
            fragmentTransaction.addToBackStack(ageScreenTag)
            fragmentTransaction.commit()
        } else {
            (activity as ActivityPatientDetailsUI).dataEditingComplete = false
            val fragment: FragmentFullDetailsScreen = FragmentFullDetailsScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.tempFragment, fragment, "FullDetailsReplace")
            fragmentTransaction.addToBackStack("FullDetailsReplace")
            fragmentTransaction.commit()
        }
    }

    private fun callPreviousFragment() {
        (activity as ActivityPatientDetailsUI).onBackPressed()
    }

    //for fragments
    private fun establishVideoConference() {
        val intent: Intent =
            Intent(this.activity as ActivityPatientDetailsUI, ActivityVideoCall::class.java)

        val getSessionBodyParam: GetSessionBodyParam =
            GetSessionBodyParam(
                (activity as ActivityPatientDetailsUI).userData.personal_details,
                (activity as ActivityPatientDetailsUI).userData.vitals,
                (activity as ActivityPatientDetailsUI).userData.doctorName
            )
        val bundle = Bundle()
        bundle.putSerializable("data", getSessionBodyParam)
        bundle.putSerializable(
            "doctorName",
            (activity as ActivityPatientDetailsUI).userData.doctorName
        )
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        intent.putExtras(bundle)

        //start video calling activity
        startActivity(intent)

        //pop back all the fragments
        Handler(Looper.getMainLooper()).postDelayed({
            //to re-initiate values
            setInitialValues()
            for (i in 0..((activity as ActivityPatientDetailsUI).supportFragmentManager.backStackEntryCount - 2)) {
                (activity as ActivityPatientDetailsUI).supportFragmentManager.popBackStack()
            }
        }, 500)
    }

    private fun setInitialValues() {
        (activity as ActivityPatientDetailsUI).dataEditingComplete = false
        //Resetting userData of ActivityPatientDetailsUI
        (activity as ActivityPatientDetailsUI).userData = UserData("")
        (activity as ActivityPatientDetailsUI).userData.bloodPressure = BloodPressure("", "")
        (activity as ActivityPatientDetailsUI).userData.vitals =
            Vitals((activity as ActivityPatientDetailsUI).userData.bloodPressure, "", "", "")
        (activity as ActivityPatientDetailsUI).userData.personal_details =
            PersonalDetails("", "", "")
        (activity as ActivityPatientDetailsUI).userData.getSessionBodyParam = GetSessionBodyParam(
            (activity as ActivityPatientDetailsUI).userData.personal_details,
            (activity as ActivityPatientDetailsUI).userData.vitals,
            (activity as ActivityPatientDetailsUI).userData.doctorName
        )
    }
}