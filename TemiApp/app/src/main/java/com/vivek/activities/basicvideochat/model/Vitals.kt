package com.vivek.activities.basicvideochat.model

import java.io.Serializable

data class Vitals(
    var blood_pressure: BloodPressure = BloodPressure(),
    var weight: String = "",
    var pulse: String = "",
    var spo2: String = "",
    var body_temp: String = ""
) : Serializable