package com.vivek.activities.basicvideochat.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.model.ServerData

class NoticeAdapter(dataList: ServerData) : RecyclerView.Adapter<NoticeAdapter.NoticeViewHolder>() {
    private val dataList: ServerData = dataList
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoticeViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.single_view_row, parent, false)
        return NoticeViewHolder(view)
    }

    override fun onBindViewHolder(holder: NoticeViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 4
    }

    inner class NoticeViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtSessionId: TextView
        var txtApiKey: TextView
        var txtToken: TextView// txtUserType;


        init {
            txtSessionId = itemView.findViewById(R.id.txt_notice_title)
            txtApiKey = itemView.findViewById(R.id.txt_notice_brief)
            txtToken = itemView.findViewById(R.id.txt_notice_file_path)
            //txtUserType =  itemView.findViewById(R.id.txt_notice_user_type);
        }
    }

}