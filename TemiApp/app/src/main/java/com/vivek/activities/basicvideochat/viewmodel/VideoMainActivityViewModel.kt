package com.vivek.activities.basicvideochat.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import com.vivek.activities.basicvideochat.model.GetSessionBodyParam
import com.vivek.activities.basicvideochat.model.ServerData
import com.vivek.activities.basicvideochat.repository.VideoMainActivityRepo
import com.vivek.activities.basicvideochat.utils.Resource

class VideoMainActivityViewModel(application: Application) : AndroidViewModel(application)
{
    private val videoMainActivityRepo: VideoMainActivityRepo = VideoMainActivityRepo()
    val videoMainActivityRepoLiveData: MediatorLiveData<Resource<ServerData>> =
        MediatorLiveData<Resource<ServerData>>()

    init {
        videoMainActivityRepoLiveData.addSource(videoMainActivityRepo.liveData){
            videoMainActivityRepoLiveData.value=it
        }
    }

    private val TAG = "VideoMainActivityViewModel"
    //TODO GetsessionMethod: will call actual method to call api from repository
    fun getSession(requestBody: GetSessionBodyParam) {
        Log.d(TAG,"inside getSession()")
            videoMainActivityRepo.getSession(requestBody)
    }
}