package com.vivek.activities.basicvideochat.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {
    private var retrofit: Retrofit? = null
    private const val BASE_URL = "http://192.168.1.173:8088/"

    /**
     * Create an instance of Retrofit object
     */
    private val retrofitInstance: Retrofit?
        get() {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit
        }
    @JvmName("getRetrofitInstance1")
    fun getRetrofitInstance(): Retrofit? {
        return retrofitInstance
    }
}