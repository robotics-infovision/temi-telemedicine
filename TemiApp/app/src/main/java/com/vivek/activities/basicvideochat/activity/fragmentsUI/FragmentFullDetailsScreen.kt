package com.vivek.activities.basicvideochat.activity.fragmentsUI

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.activity.ActivityPatientDetailsUI
import com.vivek.activities.basicvideochat.activity.ActivityVideoCall
import com.vivek.activities.basicvideochat.model.*
import com.vivek.activities.databinding.FragmentFullDetailsBinding

/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class FragmentFullDetailsScreen : Fragment() {
    private val hideHandler = Handler()

    var edtBPUpper: TextInputEditText? = null
    var edtBPLower: TextInputEditText? = null
    var edtTemp: TextInputEditText? = null
    var edtWeight: TextInputEditText? = null
    var edtPulse: TextInputEditText? = null
    var edtOxygen: TextInputEditText? = null
    var edtAge: TextInputEditText? = null
    var edtGender: String? = "GENDER"
    var edtName: TextView? = null
    var consultButton: Button? = null
    var genderImageView: ImageView? = null
    var numberBPUpper: Int = -1
    var numberBPLower: Int = -1
    var numberTemp: Float = -1.0F
    var numberWeight: Int = -1
    var numberPulse: Int = -1
    var numberOxygen: Int = -1
    var numberAge: Int = -1
    var textInputWatcherBPUpper: TextInputLayout? = null
    var textInputWatcherBPLower: TextInputLayout? = null
    var textInputWatcherTemp: TextInputLayout? = null
    var textInputWatcherWeight: TextInputLayout? = null
    var textInputWatcherPulse: TextInputLayout? = null
    var textInputWatcherOxygen: TextInputLayout? = null
    var textInputWatcherAge: TextInputLayout? = null
    var textInputWatcherButton: TextInputLayout? = null
    var llAge: LinearLayout? = null
    var llBp: LinearLayout? = null
    var llTemp: LinearLayout? = null
    var llWeight: LinearLayout? = null
    var llPulse: LinearLayout? = null
    var llOxygen: LinearLayout? = null
    var llGender: LinearLayout? = null
    public var TAG: String = "FullDetailsScreen"
    var fullDetailsScreenTag: String = "FullDetails"

    @Suppress("InlinedApi")
    private val hidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        val flags =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        activity?.window?.decorView?.systemUiVisibility = flags
        (activity as? AppCompatActivity)?.supportActionBar?.hide()
    }
    private val showPart2Runnable = Runnable {
        // Delayed display of UI elements
        fullscreenContentControls?.visibility = View.VISIBLE
    }
    private var visible: Boolean = false
//    private val hideRunnable = Runnable { hide() }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    @SuppressLint("ClickableViewAccessibility")
    private val delayHideTouchListener = View.OnTouchListener { _, _ ->
        if (AUTO_HIDE) {
//            delayedHide(AUTO_HIDE_DELAY_MILLIS)
        }
        false
    }

    private var fullscreenContent: View? = null
    private var fullscreenContentControls: View? = null

    private var _binding: FragmentFullDetailsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFullDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI(_binding!!.root.findViewById(R.id.detailsScreen))
        visible = true
        // Set up the user interaction to manually show or hide the system UI.
//        fullscreenContent?.setOnClickListener { toggle() }

//        edtBPUpper = _binding!!.root.findViewById(R.id.bpUpperEditText) as TextInputEditText
//        edtBPLower = _binding!!.root.findViewById(R.id.bpLowerEditText) as TextInputEditText
//        edtTemp = _binding!!.root.findViewById(R.id.tempEditText) as TextInputEditText
//        edtWeight = _binding!!.root.findViewById(R.id.weightEditText) as TextInputEditText
//        edtPulse = _binding!!.root.findViewById(R.id.pulseEditText) as TextInputEditText
//        edtOxygen = _binding!!.root.findViewById(R.id.oxygenEditText) as TextInputEditText
//        edtAge = _binding!!.root.findViewById(R.id.ageEditText) as TextInputEditText
//        edtName = _binding!!.root.findViewById(R.id.visitorName) as TextView

//        edtBPUpper?.setText((activity as ActivityPatientDetailsUI).userData.bloodPressure.upper)
//        edtBPLower?.setText((activity as ActivityPatientDetailsUI).userData.bloodPressure.lower)
//        edtTemp?.setText((activity as ActivityPatientDetailsUI).userData.vitals.body_temp)
//        edtWeight?.setText((activity as ActivityPatientDetailsUI).userData.vitals.weight)
//        edtPulse?.setText((activity as ActivityPatientDetailsUI).userData.vitals.pulse)
//        edtOxygen?.setText((activity as ActivityPatientDetailsUI).userData.vitals.spo2)
//        edtAge?.setText((activity as ActivityPatientDetailsUI).userData.personal_details.age)

//        textInputWatcherBPUpper =
//            _binding!!.root.findViewById(R.id.bpUpperEditTextLayout) as TextInputLayout
//        textInputWatcherBPLower =
//            _binding!!.root.findViewById(R.id.bpLowerEditTextLayout) as TextInputLayout
//        textInputWatcherAge =
//            _binding!!.root.findViewById(R.id.ageEditTextLayout) as TextInputLayout
//        textInputWatcherPulse =
//            _binding!!.root.findViewById(R.id.pulseEditTextLayout) as TextInputLayout
//        textInputWatcherWeight =
//            _binding!!.root.findViewById(R.id.weightEditTextLayout) as TextInputLayout
//        textInputWatcherOxygen =
//            _binding!!.root.findViewById(R.id.oxygenEditTextLayout) as TextInputLayout
//        textInputWatcherTemp =
//            _binding!!.root.findViewById(R.id.tempEditTextLayout) as TextInputLayout
//        textInputWatcherButton =
//            _binding!!.root.findViewById(R.id.text_input_button) as TextInputLayout

        allFunctionsOnCreated()
    }

    private fun allFunctionsOnCreated(){
        setFindViewById()
        setEditTextValues()
        setTextWatcherProperties()
        addTextChangedListeners()
        setGenderIcon()

        consultButton?.isEnabled = true
        consultButton?.isActivated = true

        setOnCLickListeners()
    }

    private fun setFindViewById() {
        edtBPUpper = _binding!!.root.findViewById(R.id.bpUpperEditText) as TextInputEditText
        edtBPLower = _binding!!.root.findViewById(R.id.bpLowerEditText) as TextInputEditText
        edtTemp = _binding!!.root.findViewById(R.id.tempEditText) as TextInputEditText
        edtWeight = _binding!!.root.findViewById(R.id.weightEditText) as TextInputEditText
        edtPulse = _binding!!.root.findViewById(R.id.pulseEditText) as TextInputEditText
        edtOxygen = _binding!!.root.findViewById(R.id.oxygenEditText) as TextInputEditText
        edtAge = _binding!!.root.findViewById(R.id.ageEditText) as TextInputEditText
        edtName = _binding!!.root.findViewById(R.id.visitorName) as TextView
        genderImageView = _binding!!.root.findViewById<View>(R.id.genderIcon) as ImageView?
        consultButton = _binding!!.root.findViewById<View>(R.id.nextButton) as Button?

        llAge = _binding!!.root.findViewById<View>(R.id.ll1) as LinearLayout
        llWeight = _binding!!.root.findViewById<View>(R.id.ll2) as LinearLayout
        llPulse = _binding!!.root.findViewById<View>(R.id.ll3) as LinearLayout
        llBp = _binding!!.root.findViewById<View>(R.id.ll4) as LinearLayout
        llTemp = _binding!!.root.findViewById<View>(R.id.ll5) as LinearLayout
        llOxygen = _binding!!.root.findViewById<View>(R.id.ll6) as LinearLayout
        llGender = _binding!!.root.findViewById<View>(R.id.llGender) as LinearLayout

        textInputWatcherBPUpper =
            _binding!!.root.findViewById(R.id.bpUpperEditTextLayout) as TextInputLayout
        textInputWatcherBPLower =
            _binding!!.root.findViewById(R.id.bpLowerEditTextLayout) as TextInputLayout
        textInputWatcherAge =
            _binding!!.root.findViewById(R.id.ageEditTextLayout) as TextInputLayout
        textInputWatcherPulse =
            _binding!!.root.findViewById(R.id.pulseEditTextLayout) as TextInputLayout
        textInputWatcherWeight =
            _binding!!.root.findViewById(R.id.weightEditTextLayout) as TextInputLayout
        textInputWatcherOxygen =
            _binding!!.root.findViewById(R.id.oxygenEditTextLayout) as TextInputLayout
        textInputWatcherTemp =
            _binding!!.root.findViewById(R.id.tempEditTextLayout) as TextInputLayout
        textInputWatcherButton =
            _binding!!.root.findViewById(R.id.text_input_button) as TextInputLayout
    }

    fun addTextChangedListeners() {
        edtBPUpper?.addTextChangedListener(vitalsText)
        edtBPLower?.addTextChangedListener(vitalsText)
        edtTemp?.addTextChangedListener(vitalsText)
        edtWeight?.addTextChangedListener(vitalsText)
        edtPulse?.addTextChangedListener(vitalsText)
        edtOxygen?.addTextChangedListener(vitalsText)
        edtAge?.addTextChangedListener(vitalsText)
//        edtName?.addTextChangedListener(vitalsText)
    }

    private fun setEditTextValues() {
        edtBPUpper?.setText((activity as ActivityPatientDetailsUI).userData.bloodPressure.upper)
        edtBPLower?.setText((activity as ActivityPatientDetailsUI).userData.bloodPressure.lower)
        edtTemp?.setText((activity as ActivityPatientDetailsUI).userData.vitals.body_temp)
        edtWeight?.setText((activity as ActivityPatientDetailsUI).userData.vitals.weight)
        edtPulse?.setText((activity as ActivityPatientDetailsUI).userData.vitals.pulse)
        edtOxygen?.setText((activity as ActivityPatientDetailsUI).userData.vitals.spo2)
        edtAge?.setText((activity as ActivityPatientDetailsUI).userData.personal_details.age)
        edtName?.text = (activity as ActivityPatientDetailsUI).userData.personal_details.name + ", "
    }

    fun setOnCLickListeners() {
        consultButton?.setOnClickListener {
            if (consultButton?.isActivated == true && consultButton?.isEnabled == true) {
                consultButton?.isEnabled = true
                consultButton?.isActivated = true
                textInputWatcherButton?.isErrorEnabled = false
                Log.i(TAG, "consult button clicked")
                establishVideoConference()
            }
        }
        llAge?.setOnClickListener {
            Log.i("fragmentAge", "fragment age clicked")
            (activity as ActivityPatientDetailsUI).dataEditingComplete = true
            val fragment: FragmentAgeScreen = FragmentAgeScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.tempFragment, fragment, "SecondAgeScreen")
            fragmentTransaction.addToBackStack("SecondAgeScreen")
            fragmentTransaction.commit()
        }
        llWeight?.setOnClickListener {
            (activity as ActivityPatientDetailsUI).dataEditingComplete = true
            val fragment: FragmentWeightScreen = FragmentWeightScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.tempFragment, fragment, "SecondWeightScreen")
            fragmentTransaction.addToBackStack("SecondWeightScreen")
            fragmentTransaction.commit()
        }
        llPulse?.setOnClickListener {
            (activity as ActivityPatientDetailsUI).dataEditingComplete = true
            val fragment: FragmentPulseScreen = FragmentPulseScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.tempFragment, fragment, "SecondPulseScreen")
            fragmentTransaction.addToBackStack("SecondPulseScreen")
            fragmentTransaction.commit()
        }
        llBp?.setOnClickListener {
            (activity as ActivityPatientDetailsUI).dataEditingComplete = true
            val fragment: FragmentBloodPressureScreen = FragmentBloodPressureScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.tempFragment, fragment, "SecondBPScreen")
            fragmentTransaction.addToBackStack("SecondBPScreen")
            fragmentTransaction.commit()
        }
        llTemp?.setOnClickListener {
            (activity as ActivityPatientDetailsUI).dataEditingComplete = true
            val fragment: FragmentTemperatureScreen = FragmentTemperatureScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.tempFragment, fragment, "SecondTempScreen")
            fragmentTransaction.addToBackStack("SecondTempScreen")
            fragmentTransaction.commit()
        }
        llOxygen?.setOnClickListener {
            (activity as ActivityPatientDetailsUI).dataEditingComplete = true
            val fragment: FragmentOxygenScreen = FragmentOxygenScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.tempFragment, fragment, "SecondOxygenScreen")
            fragmentTransaction.addToBackStack("SecondOxygenScreen")
            fragmentTransaction.commit()
        }
        llGender?.setOnClickListener {
            (activity as ActivityPatientDetailsUI).dataEditingComplete = true
            val fragment: FragmentGenderScreen = FragmentGenderScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.tempFragment, fragment, "SecondGenderScreen")
            fragmentTransaction.addToBackStack("SecondGenderScreen")
            fragmentTransaction.commit()
        }
    }

    private fun setGenderIcon(){
        if ((activity as ActivityPatientDetailsUI).userData.personal_details.gender == "FEMALE") {
            genderImageView?.setImageDrawable(((context?.let {
                androidx.core.content.ContextCompat.getDrawable(
                    it,
                    R.drawable.icon_female
                )
            })))
            edtGender = "FEMALE"
        } else {
            genderImageView?.setImageDrawable(((context?.let {
                androidx.core.content.ContextCompat.getDrawable(
                    it,
                    R.drawable.icon_male
                )
            })))
            edtGender = "MALE"
        }
    }

    private val vitalsText: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            (activity as ActivityPatientDetailsUI).userData.personal_details.age =
                edtAge?.text.toString()
            (activity as ActivityPatientDetailsUI).userData.vitals.spo2 = edtOxygen?.text.toString()
            (activity as ActivityPatientDetailsUI).userData.vitals.pulse = edtPulse?.text.toString()
            (activity as ActivityPatientDetailsUI).userData.vitals.weight =
                edtWeight?.text.toString()
            (activity as ActivityPatientDetailsUI).userData.vitals.blood_pressure.upper =
                edtBPUpper?.text.toString()
            (activity as ActivityPatientDetailsUI).userData.vitals.blood_pressure.lower =
                edtBPLower?.text.toString()
            (activity as ActivityPatientDetailsUI).userData.vitals.body_temp =
                edtTemp?.text.toString()
            val bloodPressure: BloodPressure = BloodPressure(
                (activity as ActivityPatientDetailsUI).userData.vitals.blood_pressure.upper,
                (activity as ActivityPatientDetailsUI).userData.vitals.blood_pressure.lower
            )
            val personal_Details: PersonalDetails = PersonalDetails(
                (activity as ActivityPatientDetailsUI).userData.personal_details.name,
                (activity as ActivityPatientDetailsUI).userData.personal_details.age,
                (activity as ActivityPatientDetailsUI).userData.personal_details.gender
            )
            val vitals: Vitals = Vitals(
                bloodPressure, (activity as ActivityPatientDetailsUI).userData.vitals.weight,
                (activity as ActivityPatientDetailsUI).userData.vitals.pulse,
                (activity as ActivityPatientDetailsUI).userData.vitals.spo2,
                (activity as ActivityPatientDetailsUI).userData.vitals.body_temp
            )

            (activity as ActivityPatientDetailsUI).userData.vitals = vitals
            (activity as ActivityPatientDetailsUI).userData.getSessionBodyParam =
                GetSessionBodyParam(
                    personal_Details, vitals,
                    (activity as ActivityPatientDetailsUI).userData.doctorName
                )

            if (edtAge?.text.toString() != "" &&
                edtWeight?.text.toString() != "" &&
                edtPulse?.text.toString() != "" &&
                edtBPLower?.text.toString() != "" &&
                edtBPUpper?.text.toString() != "" &&
                edtOxygen?.text.toString() != "" &&
                edtTemp?.text.toString() != ""
            ) {
                consultButton?.isEnabled = true
                consultButton?.isActivated = true
            }
            if (edtAge?.text.toString() == "" ||
                edtWeight?.text.toString() == "" ||
                edtPulse?.text.toString() == "" ||
                edtBPLower?.text.toString() == "" ||
                edtBPUpper?.text.toString() == "" ||
                edtOxygen?.text.toString() == "" ||
                edtTemp?.text.toString() == ""
            ) {
                consultButton?.isEnabled = false
                consultButton?.isActivated = false
            }
            //turning data to integer
//            setNumbersQuantities()

            //radio button for gender male female other
            //button will be enabled after all fields are filled
//            consultButton?.isEnabled = edtBPUpper?.text!!.isNotEmpty() &&
//                    edtBPLower?.text!!.isNotEmpty() &&
//                    edtAge?.text!!.isNotEmpty() &&
//                    edtOxygen?.text!!.isNotEmpty() &&
//                    edtTemp?.text!!.isNotEmpty() &&
//                    edtWeight?.text!!.isNotEmpty() &&
//                    edtPulse?.text!!.isNotEmpty() &&
////                    edtName?.text!!.isNotEmpty() &&
//                    edtGender != "GENDER"
//            consultButton?.isActivated = consultButton?.isEnabled!!

            //applying conditions on user input
//            if (numberAge != -1) {
//                if (numberAge < Constants.age_lower) {
//                    textInputWatcherAge?.isErrorEnabled = true
//                    textInputWatcherAge?.error = "Enter field above ${Constants.age_lower}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else if (numberAge > Constants.age_higher) {
//                    textInputWatcherAge?.isErrorEnabled = true
//                    textInputWatcherAge?.error = "Enter field below ${Constants.age_higher}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else {
//                    textInputWatcherAge?.isErrorEnabled = false
//                }
//            } else {
//                consultButton?.isEnabled = false
//                consultButton?.isActivated = false
//            }
//
//            if (numberOxygen != -1) {
//                if (numberOxygen < Constants.spo2_lower) {
//                    textInputWatcherOxygen?.isErrorEnabled = true
//                    textInputWatcherOxygen?.error =
//                        "Enter field above ${Constants.spo2_lower}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else if (numberOxygen > Constants.spo2_higher) {
//                    textInputWatcherOxygen?.isErrorEnabled = true
//                    textInputWatcherOxygen?.error =
//                        "Enter field below ${Constants.spo2_higher}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else {
//                    textInputWatcherOxygen?.isErrorEnabled = false
//                }
//            } else {
//                consultButton?.isEnabled = false
//                consultButton?.isActivated = false
//            }
//
//            if (numberPulse != -1) {
//                if (numberPulse < Constants.pulse_lower) {
//                    textInputWatcherPulse?.isErrorEnabled = true
//                    textInputWatcherPulse?.error =
//                        "Enter field above ${Constants.pulse_lower}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else if (numberPulse > Constants.pulse_higher) {
//                    textInputWatcherPulse?.isErrorEnabled = true
//                    textInputWatcherPulse?.error =
//                        "Enter field below ${Constants.pulse_higher}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else {
//                    textInputWatcherPulse?.isErrorEnabled = false
//                }
//            } else {
//                consultButton?.isEnabled = false
//                consultButton?.isActivated = false
//            }
//
//            if (numberTemp != -1.0F) {
//                if (numberTemp < Constants.temperature_lower) {
//                    textInputWatcherTemp?.isErrorEnabled = true
//                    textInputWatcherTemp?.error =
//                        "Enter field above ${Constants.temperature_lower}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else if (numberTemp > Constants.temperature_higher) {
//                    textInputWatcherTemp?.isErrorEnabled = true
//                    textInputWatcherTemp?.error =
//                        "Enter field below ${Constants.temperature_higher}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else {
//                    textInputWatcherTemp?.isErrorEnabled = false
//                }
//            } else {
//                consultButton?.isEnabled = false
//                consultButton?.isActivated = false
//            }
//
//            if (numberWeight != -1) {
//                if (numberWeight < Constants.weight_lower
//                ) {
//                    textInputWatcherWeight?.isErrorEnabled = true
//                    textInputWatcherWeight?.error =
//                        "Enter field above ${Constants.weight_lower}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else if (numberWeight > Constants.weight_higher
//                ) {
//                    textInputWatcherWeight?.isErrorEnabled = true
//                    textInputWatcherWeight?.error =
//                        "Enter field below ${Constants.weight_higher}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else {
//                    textInputWatcherWeight?.isErrorEnabled = false
//                }
//            } else {
//                consultButton?.isEnabled = false
//                consultButton?.isActivated = false
//            }
//
//            if (numberBPUpper != -1) {
//                if (numberBPUpper > Constants.blood_pressure_higher) {
//                    textInputWatcherBPUpper?.isErrorEnabled = true
//                    textInputWatcherBPUpper?.error =
//                        "Enter field below ${Constants.blood_pressure_higher}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else {
//                    textInputWatcherBPUpper?.isErrorEnabled = false
//                }
//            } else {
//                consultButton?.isEnabled = false
//                consultButton?.isActivated = false
//            }
//
//            if (numberBPLower != -1) {
//                if (numberBPLower < Constants.blood_pressure_lower) {
//                    textInputWatcherBPLower?.isErrorEnabled = true
//                    textInputWatcherBPLower?.error =
//                        "Enter field above ${Constants.blood_pressure_lower}"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else {
//                    textInputWatcherBPLower?.isErrorEnabled = false
//                }
//            } else {
//                consultButton?.isEnabled = false
//                consultButton?.isActivated = false
//            }
//
//            if (numberBPLower != -1 && numberBPUpper != -1) {
//                if (numberBPUpper < numberBPLower) {
//                    textInputWatcherBPUpper?.isErrorEnabled = true
//                    textInputWatcherBPUpper?.error =
//                        "Lower Blood Pressure cannot be higher than Higher Blood Pressure"
//                    textInputWatcherBPLower?.isErrorEnabled = true
//                    textInputWatcherBPLower?.error =
//                        "Higher Blood Pressure cannot be lower than Lower Blood Pressure"
//                    consultButton?.isEnabled = false
//                    consultButton?.isActivated = consultButton?.isEnabled!!
//                } else {
//                    textInputWatcherBPUpper?.isErrorEnabled = false
//                    textInputWatcherBPLower?.isErrorEnabled = false
//                }
//            } else {
//                consultButton?.isEnabled = false
//                consultButton?.isActivated = false
//            }
//
//            //changing error and hint on button
//            if (textInputWatcherBPLower?.isErrorEnabled == false &&
//                textInputWatcherBPUpper?.isErrorEnabled == false &&
//                textInputWatcherTemp?.isErrorEnabled == false &&
//                textInputWatcherOxygen?.isErrorEnabled == false &&
//                textInputWatcherPulse?.isErrorEnabled == false &&
//                textInputWatcherWeight?.isErrorEnabled == false &&
//                textInputWatcherAge?.isErrorEnabled == false &&
//                edtGender != "GENDER"
//            ) {
//                textInputWatcherButton?.error = null
//            }
        }

        override fun afterTextChanged(s: Editable) {
        }
    }

    //setting textwatcher properties
    private fun setTextWatcherProperties() {

        textInputWatcherBPUpper?.isHelperTextEnabled = false
        textInputWatcherBPLower?.isHelperTextEnabled = false
        textInputWatcherTemp?.isHelperTextEnabled = false
        textInputWatcherWeight?.isHelperTextEnabled = false
        textInputWatcherPulse?.isHelperTextEnabled = false
        textInputWatcherOxygen?.isHelperTextEnabled = false
        textInputWatcherAge?.isHelperTextEnabled = false

        textInputWatcherBPUpper?.isErrorEnabled = false
        textInputWatcherBPLower?.isErrorEnabled = false
        textInputWatcherTemp?.isErrorEnabled = false
        textInputWatcherWeight?.isErrorEnabled = false
        textInputWatcherPulse?.isErrorEnabled = false
        textInputWatcherOxygen?.isErrorEnabled = false
        textInputWatcherAge?.isErrorEnabled = false
        textInputWatcherButton?.isErrorEnabled = false
    }

    private fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager: InputMethodManager = activity.getSystemService(
            AppCompatActivity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(
                activity.currentFocus!!.windowToken,
                0
            )
        }
    }

    //setting user input values
    private fun setNumbersQuantities() {

        //Age number
        if (edtAge?.text!!.toString() == "") {
            numberAge = -1
        } else {
            numberAge = Integer.parseInt(edtAge?.text!!.toString())
        }
        //BPUpper number
        if (edtBPUpper?.text!!.toString() == "") {
            numberBPUpper = -1
        } else {
            numberBPUpper = Integer.parseInt(edtBPUpper?.text!!.toString())
        }
        //BPLower number
        if (edtBPLower?.text!!.toString() == "") {
            numberBPLower = -1
        } else {
            numberBPLower = Integer.parseInt(edtBPLower?.text!!.toString())
        }
        //Oxygen number
        if (edtOxygen?.text!!.toString() == "") {
            numberOxygen = -1
        } else {
            numberOxygen = Integer.parseInt(edtOxygen?.text!!.toString())
        }
        //Temp number
        if (edtTemp?.text!!.toString() == "") {
            numberTemp = -1.0F
        } else {
            numberTemp = (edtTemp?.text!!.toString()).toFloat()
        }
        //Weight number
        if (edtWeight?.text!!.toString() == "") {
            numberWeight = -1
        } else {
            numberWeight = Integer.parseInt(edtWeight?.text!!.toString())
        }
        //Pulse number
        if (edtPulse?.text!!.toString() == "") {
            numberPulse = -1
        } else {
            numberPulse = Integer.parseInt(edtPulse?.text!!.toString())
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    fun setupUI(view: View) {
        // Set up touch listener for non-text box views to hide keyboard.
//        view !is EditText &&
        if (view !is LinearLayout) {
            view.setOnTouchListener { _, _ ->
                hideSoftKeyboard(activity as ActivityPatientDetailsUI)
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                setupUI(innerView)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        allFunctionsOnCreated()
        Log.i("Age on Resume", (activity as ActivityPatientDetailsUI).userData.personal_details.age)
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
//        delayedHide(100)
    }

    override fun onPause() {
        super.onPause()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        // Clear the systemUiVisibility flag
        activity?.window?.decorView?.systemUiVisibility = 0
//        show()
    }

    override fun onDestroy() {
        super.onDestroy()
        fullscreenContent = null
        fullscreenContentControls = null
    }

//    private fun toggle() {
//        if (visible) {
//            hide()
//        } else {
//            show()
//        }
//    }

//    private fun hide() {
//        // Hide UI first
//        fullscreenContentControls?.visibility = View.GONE
//        visible = false
//
//        // Schedule a runnable to remove the status and navigation bar after a delay
//        hideHandler.removeCallbacks(showPart2Runnable)
//        hideHandler.postDelayed(hidePart2Runnable, UI_ANIMATION_DELAY.toLong())
//    }
//
//    @Suppress("InlinedApi")
//    private fun show() {
//        // Show the system bar
//        fullscreenContent?.systemUiVisibility =
//            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
//                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//        visible = true
//
//        // Schedule a runnable to display UI elements after a delay
//        hideHandler.removeCallbacks(hidePart2Runnable)
//        hideHandler.postDelayed(showPart2Runnable, UI_ANIMATION_DELAY.toLong())
//        (activity as? AppCompatActivity)?.supportActionBar?.show()
//    }
//
//    /**
//     * Schedules a call to hide() in [delayMillis], canceling any
//     * previously scheduled calls.
//     */
//    private fun delayedHide(delayMillis: Int) {
//        hideHandler.removeCallbacks(hideRunnable)
//        hideHandler.postDelayed(hideRunnable, delayMillis.toLong())
//    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private const val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private const val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private const val UI_ANIMATION_DELAY = 300
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun establishVideoConference() {
        val intent: Intent =
            Intent(this.activity as ActivityPatientDetailsUI, ActivityVideoCall::class.java)

//        val blood_pressure: BloodPressure = BloodPressure("80", "120")
//        val personal_details: PersonalDetails = PersonalDetails("vivek_trial", "26", "MALE")
//        val vitals: Vitals = Vitals(blood_pressure, "70", "72", "100", "98")

        val getSessionBodyParam: GetSessionBodyParam =
            GetSessionBodyParam(
                (activity as ActivityPatientDetailsUI).userData.personal_details,
                (activity as ActivityPatientDetailsUI).userData.vitals,
                (activity as ActivityPatientDetailsUI).userData.doctorName
            )
        val bundle = Bundle()
        bundle.putSerializable("data", getSessionBodyParam)
        bundle.putSerializable(
            "doctorName",
            (activity as ActivityPatientDetailsUI).userData.doctorName
        )
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        intent.putExtras(bundle)

        //start video calling activity
        startActivity(intent)

        //pop back all the fragments
        Handler(Looper.getMainLooper()).postDelayed({
            //to re-initiate values
            setInitialValues()
            for (i in 0..((activity as ActivityPatientDetailsUI).supportFragmentManager.backStackEntryCount - 2)) {
                (activity as ActivityPatientDetailsUI).supportFragmentManager.popBackStack()
            }
        }, 500)
    }

    private fun setInitialValues() {
        edtBPUpper?.text = null
        edtBPLower?.text = null
        edtTemp?.text = null
        edtWeight?.text = null
        edtPulse?.text = null
        edtOxygen?.text = null
        edtAge?.text = null
        edtName?.text = null
        edtGender = "GENDER"

        (activity as ActivityPatientDetailsUI).dataEditingComplete = false
        //Resetting userData of ActivityPatientDetailsUI
        (activity as ActivityPatientDetailsUI).userData = UserData("")
        (activity as ActivityPatientDetailsUI).userData.bloodPressure = BloodPressure("", "")
        (activity as ActivityPatientDetailsUI).userData.vitals =
            Vitals((activity as ActivityPatientDetailsUI).userData.bloodPressure, "", "", "")
        (activity as ActivityPatientDetailsUI).userData.personal_details =
            PersonalDetails("", "", "")
        (activity as ActivityPatientDetailsUI).userData.getSessionBodyParam = GetSessionBodyParam(
            (activity as ActivityPatientDetailsUI).userData.personal_details,
            (activity as ActivityPatientDetailsUI).userData.vitals,
            (activity as ActivityPatientDetailsUI).userData.doctorName
        )
    }


}