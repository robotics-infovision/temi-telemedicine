package com.vivek.activities.temi

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.robotemi.sdk.Robot
import com.robotemi.sdk.TtsRequest
import com.robotemi.sdk.listeners.OnConversationStatusChangedListener
import com.robotemi.sdk.listeners.OnDetectionDataChangedListener
import com.robotemi.sdk.listeners.OnDetectionStateChangedListener
import com.robotemi.sdk.listeners.OnUserInteractionChangedListener
import com.robotemi.sdk.model.DetectionData
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.activity.ActivityVideoCall

class UIMainActivity : AppCompatActivity(), OnDetectionStateChangedListener,
    OnUserInteractionChangedListener, OnDetectionDataChangedListener,
    OnConversationStatusChangedListener, Robot.AsrListener {
    var robot: Robot? = null
    var button: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.temi_telemedicine)
        button = findViewById(R.id.btn_call_doctor)

        //robot functions
        robot = Robot.getInstance()
        robot!!.setKioskModeOn(true)
        robot!!.hideTopBar()
        robot!!.detectionModeOn = true
        Log.i("Robot: Detection", robot!!.detectionModeOn.toString())
        if (robot!!.detectionModeOn) {
            Log.i("Robot: Detection if condition", "yes ture")
        }
        robot!!.trackUserOn = true
        Log.i("Robot: Track User", robot!!.trackUserOn.toString())
        if (robot!!.trackUserOn) {
            Log.i("Robot: Track if condition", "yes ture")
        }
        button!!.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@UIMainActivity, com.vivek.activities.videocall.activity.VideoMainActivity::class.java)
            startActivity(intent)
        })
    }

    public override fun onStart() {
        super.onStart()
        robot!!.addOnDetectionStateChangedListener(this)
        robot!!.addOnUserInteractionChangedListener(this)
        robot!!.addAsrListener(this)
    }

    public override fun onStop() {
        robot!!.removeOnDetectionStateChangedListener(this)
        robot!!.removeOnUserInteractionChangedListener(this)
        robot!!.removeAsrListener(this)
        super.onStop()
    }

    override fun onDetectionStateChanged(i: Int) {
        if (i == 2) {
            val to_say = findViewById<TextView>(R.id.to_say)
            val ttsRequest = TtsRequest.create(
                "Do you want me to call the doctor?",
                false, TtsRequest.Language.EN_US
            )
            robot!!.speak(ttsRequest)
            to_say.text = "Do you want me to call the doctor?"
            robot!!.askQuestion("Do you want me to call the doctor?")
        }
    }

    override fun onUserInteraction(b: Boolean) {}
    override fun onDetectionDataChanged(detectionData: DetectionData) {}
    override fun onAsrResult(s: String) {
        if (s.equals("yes", ignoreCase = true)) {
            val intent = Intent(this@UIMainActivity, ActivityVideoCall::class.java)
            startActivity(intent)
        } else if (s.equals("no", ignoreCase = true)) {
            val ttsRequest = TtsRequest.create(
                "Please go to waiting room while we send someone to assist you",
                false, TtsRequest.Language.EN_US
            )
            robot!!.speak(ttsRequest)
            val to_say = findViewById<TextView>(R.id.to_say)
            to_say.text = "Please go to waiting room while we send someone to assist you"
        } else {
            robot!!.askQuestion("Sorry! I could not understand that. Can you please repeat that?")
        }
    }

    override fun onConversationStatusChanged(i: Int, s: String) {}
}