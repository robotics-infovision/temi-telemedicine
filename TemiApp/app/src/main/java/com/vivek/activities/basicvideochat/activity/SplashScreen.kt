package com.vivek.activities.basicvideochat.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vivek.activities.R

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)
        val intent = Intent(this@SplashScreen, UIMainActivity::class.java)
//        var handler: Handler = Handler()
//        handler.postDelayed(
//            {
        startActivity(intent)
//            },3000)
    }
}