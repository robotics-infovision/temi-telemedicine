package com.vivek.activities.basicvideochat.activity.fragmentsUI

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.aigestudio.wheelpicker.WheelPicker
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.activity.ActivityPatientDetailsUI
import com.vivek.activities.basicvideochat.activity.ActivityVideoCall
import com.vivek.activities.basicvideochat.model.*
import com.vivek.activities.databinding.FragmentBloodPressureScreenBinding


/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class FragmentBloodPressureScreen : Fragment(), WheelPicker.OnItemSelectedListener,
    View.OnClickListener {
    private val hideHandler = Handler()

    private var wheelLeft: WheelPicker? = null
    private var wheelRight: WheelPicker? = null

    val bp1: MutableList<Int> = mutableListOf()
    var databp1: List<Int>? = null;
    val bp2: MutableList<Int> = mutableListOf()
    var databp2: List<Int>? = null;
    var imageButtonNext: ImageButton? = null
    var imageButtonBack: ImageButton? = null
    var leftWheelValue: Int = 80
    var rightWheelValue: Int = 120
    var leftWheelPosition: Int = 10
    var rightWheelPosition: Int = 50
    var leftWheelTouched: Boolean = false
    var rightWheelTouched: Boolean = false
    var bpTextView: TextView? = null
    public var TAG: String = "BloodPressureScreen"
    var bpScreenTag: String = "BP"
    var consultButton: Button? = null
    var summaryButton: Button? = null

    private var visible: Boolean = false

    private var fullscreenContent: View? = null
    private var fullscreenContentControls: View? = null

    //    private var gotoBtn: Button? = null
    private var gotoBtnItemIndex: Int? = null
    private var _binding: FragmentBloodPressureScreenBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBloodPressureScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        visible = true
        allFunctionsOnCreated()
    }

    private fun allFunctionsOnCreated() {
        setVisiblityConsultationButton()
        setFindViewById()
        setCustomizedData()
        setOnClickListeners()
    }

    private fun setFindViewById() {
        wheelLeft = _binding!!.root.findViewById<View>(R.id.main_wheel_left) as WheelPicker
        wheelRight = _binding!!.root.findViewById<View>(R.id.main_wheel_right) as WheelPicker
        bpTextView = _binding!!.root.findViewById<View>(R.id.bpNormalRange) as TextView
        imageButtonNext = _binding!!.root.findViewById<View>(R.id.nextButton) as ImageButton?
        imageButtonBack = _binding!!.root.findViewById<View>(R.id.backButton) as ImageButton?
    }

    private fun setCustomizedData() {
        bpTextView!!.text = "(Normal Range: 80-120 mmHg)"
        for (i in 50..180) {
            bp1.add(i - 50, i)
        }
        databp1 = bp1
        wheelLeft?.data = databp1
        for (i in 50..180) {
            bp2.add(i - 50, i)
        }
        databp2 = bp2
        wheelRight?.data = databp2
        //set custom data according to user personal details
        if (Integer.parseInt((activity as ActivityPatientDetailsUI).userData.personal_details.age) >= 18) {
            if ((activity as ActivityPatientDetailsUI).userData.personal_details.gender == "MALE") {
                if (Integer.parseInt((activity as ActivityPatientDetailsUI).userData.personal_details.age) in 18..39) {
                    wheelLeft?.selectedItemPosition = 20
                    wheelLeft?.setSelectedItemPosition(20, true)

                    wheelRight?.selectedItemPosition = 69
                    wheelRight?.setSelectedItemPosition(69, true)

                    bpTextView!!.text = "(Normal Range: 70-119 mmHg)"
                } else if (Integer.parseInt((activity as ActivityPatientDetailsUI).userData.personal_details.age) in 40..59) {
                    wheelLeft?.selectedItemPosition = 27
                    wheelLeft?.setSelectedItemPosition(27, true)

                    wheelRight?.selectedItemPosition = 74
                    wheelRight?.setSelectedItemPosition(74, true)

                    bpTextView!!.text = "(Normal Range: 77-124 mmHg)"
                } else if (Integer.parseInt((activity as ActivityPatientDetailsUI).userData.personal_details.age) >= 60) {
                    wheelLeft?.selectedItemPosition = 19
                    wheelLeft?.setSelectedItemPosition(19, true)

                    wheelRight?.selectedItemPosition = 83
                    wheelRight?.setSelectedItemPosition(83, true)

                    bpTextView!!.text = "(Normal Range: 69-133 mmHg)"
                } else {
                    wheelLeft?.selectedItemPosition = 30
                    wheelLeft?.setSelectedItemPosition(30, true)

                    wheelRight?.selectedItemPosition = 70
                    wheelRight?.setSelectedItemPosition(70, true)

                    bpTextView!!.text = "(Normal Range: 80-120 mmHg)"
                }
            } else {
                if (Integer.parseInt((activity as ActivityPatientDetailsUI).userData.personal_details.age) in 18..39) {
                    wheelLeft?.selectedItemPosition = 18
                    wheelLeft?.setSelectedItemPosition(18, true)

                    wheelRight?.selectedItemPosition = 60
                    wheelRight?.setSelectedItemPosition(60, true)

                    bpTextView!!.text = "(Normal Range: 68-110 mmHg)"
                } else if (Integer.parseInt((activity as ActivityPatientDetailsUI).userData.personal_details.age) in 40..59) {
                    wheelLeft?.selectedItemPosition = 24
                    wheelLeft?.setSelectedItemPosition(24, true)

                    wheelRight?.selectedItemPosition = 72
                    wheelRight?.setSelectedItemPosition(72, true)

                    bpTextView!!.text = "(Normal Range: 74-122 mmHg)"
                } else if (Integer.parseInt((activity as ActivityPatientDetailsUI).userData.personal_details.age) >= 60) {
                    wheelLeft?.selectedItemPosition = 18
                    wheelLeft?.setSelectedItemPosition(18, true)

                    wheelRight?.selectedItemPosition = 89
                    wheelRight?.setSelectedItemPosition(89, true)

                    bpTextView!!.text = "(Normal Range: 68-139 mmHg)"
                } else {
                    wheelLeft?.selectedItemPosition = 30
                    wheelLeft?.setSelectedItemPosition(30, true)

                    wheelRight?.selectedItemPosition = 70
                    wheelRight?.setSelectedItemPosition(70, true)

                    bpTextView!!.text = "(Normal Range: 80-120 mmHg)"
                }
            }
        } else {
            wheelLeft?.selectedItemPosition = 3
            wheelLeft?.setSelectedItemPosition(3, true)

            wheelRight?.selectedItemPosition = 78
            wheelRight?.setSelectedItemPosition(78, true)

            bpTextView!!.text = "(Normal Range: 80-120 mmHg)"
        }
        imageButtonNext?.setImageDrawable(((context?.let {
            androidx.core.content.ContextCompat.getDrawable(
                it,
                R.drawable.icon_next_disable
            )
        })))
        imageButtonNext?.isActivated = false
        imageButtonNext?.isEnabled = false
    }

    fun setOnClickListeners() {
        wheelLeft!!.setOnItemSelectedListener(this)
        wheelRight!!.setOnItemSelectedListener(this)
        imageButtonNext?.setOnClickListener {
            if (imageButtonNext?.isActivated == true &&
                imageButtonNext?.isEnabled == true
            ) {
                Log.i(TAG, "next button clicked")
                callNextFragment()
            }
        }
        imageButtonBack?.setOnClickListener {
            Log.i(TAG, "back button clicked")
            callPreviousFragment()
        }
        if ((activity as ActivityPatientDetailsUI).dataEditingComplete) {
            setCurrentState()
        }
    }

    private fun setVisiblityConsultationButton() {
        if (!(activity as ActivityPatientDetailsUI).dataEditingComplete) {
            (_binding!!.root.findViewById<View>(R.id.consultButton) as Button).visibility =
                View.GONE
            (_binding!!.root.findViewById<View>(R.id.backToSummary) as Button).visibility =
                View.GONE
        } else {
            (_binding!!.root.findViewById<View>(R.id.nextButton) as ImageButton).visibility =
                View.GONE
            consultButton = _binding!!.root.findViewById<View>(R.id.consultButton) as Button
            summaryButton = _binding!!.root.findViewById<View>(R.id.backToSummary) as Button
            consultButton?.setOnClickListener {
                    establishVideoConference()
            }
            summaryButton?.setOnClickListener {
                callNextFragment()
            }
        }
    }

    private fun setCurrentState() {
        wheelLeft?.selectedItemPosition =
            Integer.parseInt((activity as ActivityPatientDetailsUI).userData.vitals.blood_pressure.lower) - 50
        wheelLeft?.setSelectedItemPosition(
            Integer.parseInt((activity as ActivityPatientDetailsUI).userData.vitals.blood_pressure.lower) - 50,
            true
        )
        wheelRight?.selectedItemPosition =
            Integer.parseInt((activity as ActivityPatientDetailsUI).userData.vitals.blood_pressure.upper) - 50
        wheelRight?.setSelectedItemPosition(
            Integer.parseInt((activity as ActivityPatientDetailsUI).userData.vitals.blood_pressure.upper) - 50,
            true
        )
    }

    override fun onResume() {
        super.onResume()
        allFunctionsOnCreated()
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }

    override fun onPause() {
        super.onPause()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        // Clear the systemUiVisibility flag
        activity?.window?.decorView?.systemUiVisibility = 0
    }

    override fun onDestroy() {
        super.onDestroy()
        fullscreenContent = null
        fullscreenContentControls = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemSelected(picker: WheelPicker, data: Any, position: Int) {
        var text = ""
        when (picker.id) {
            R.id.main_wheel_left -> {
                text = "Left:"
                leftWheelPosition = position
                leftWheelValue = Integer.parseInt(data.toString())
                (activity as ActivityPatientDetailsUI).userData.bloodPressure.lower =
                    leftWheelValue.toString()
                (activity as ActivityPatientDetailsUI).userData.bloodPressure.upper =
                    rightWheelValue.toString()
                leftWheelTouched = true
                if (leftWheelValue > rightWheelValue) {
                    wheelRight?.selectedItemPosition = leftWheelPosition
                    rightWheelPosition = leftWheelPosition
                    rightWheelValue = leftWheelValue
                    (activity as ActivityPatientDetailsUI).userData.bloodPressure.lower =
                        leftWheelValue.toString()
                    (activity as ActivityPatientDetailsUI).userData.bloodPressure.upper =
                        rightWheelValue.toString()
                }
            }
            R.id.main_wheel_right -> {
                text = "Right:"
                rightWheelPosition = position
                rightWheelValue = Integer.parseInt(data.toString())
                (activity as ActivityPatientDetailsUI).userData.bloodPressure.lower =
                    leftWheelValue.toString()
                (activity as ActivityPatientDetailsUI).userData.bloodPressure.upper =
                    rightWheelValue.toString()
                rightWheelTouched = true
                if (leftWheelValue > rightWheelValue && leftWheelTouched) {
                    wheelLeft?.selectedItemPosition = rightWheelPosition
                    leftWheelPosition = rightWheelPosition
                    leftWheelValue = rightWheelValue
                    (activity as ActivityPatientDetailsUI).userData.bloodPressure.lower =
                        leftWheelValue.toString()
                    (activity as ActivityPatientDetailsUI).userData.bloodPressure.upper =
                        rightWheelValue.toString()
                }
            }
        }
        if (leftWheelTouched && rightWheelTouched) {
            imageButtonNext?.isActivated = true
            imageButtonNext?.isEnabled = true
            consultButton?.isActivated = true
            consultButton?.isEnabled = true
            imageButtonNext?.setImageDrawable(((context?.let {
                androidx.core.content.ContextCompat.getDrawable(
                    it,
                    R.drawable.icon_next_enable
                )
            })))
        }
        Log.i(
            TAG,
            "Lower BP: ${(activity as ActivityPatientDetailsUI).userData.bloodPressure.lower} : Upper BP: ${(activity as ActivityPatientDetailsUI).userData.bloodPressure.upper}"
        )
    }

    override fun onClick(v: View?) {
        wheelLeft?.selectedItemPosition = leftWheelPosition
        wheelRight?.selectedItemPosition = rightWheelPosition
    }

    private fun callNextFragment() {
        if (!(activity as ActivityPatientDetailsUI).dataEditingComplete) {
            val fragment: FragmentTemperatureScreen = FragmentTemperatureScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.tempFragment, fragment, bpScreenTag)
            fragmentTransaction.addToBackStack(bpScreenTag)
            fragmentTransaction.commit()
        } else {
            (activity as ActivityPatientDetailsUI).dataEditingComplete = false
            val fragment: FragmentFullDetailsScreen = FragmentFullDetailsScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.tempFragment, fragment, "FullDetailsReplace")
            fragmentTransaction.addToBackStack("FullDetailsReplace")
            fragmentTransaction.commit()
        }
    }

    private fun callPreviousFragment() {
        (activity as ActivityPatientDetailsUI).onBackPressed()
    }

    //for fragments
    private fun establishVideoConference() {
        val intent: Intent =
            Intent(this.activity as ActivityPatientDetailsUI, ActivityVideoCall::class.java)

        val getSessionBodyParam: GetSessionBodyParam =
            GetSessionBodyParam(
                (activity as ActivityPatientDetailsUI).userData.personal_details,
                (activity as ActivityPatientDetailsUI).userData.vitals,
                (activity as ActivityPatientDetailsUI).userData.doctorName
            )
        val bundle = Bundle()
        bundle.putSerializable("data", getSessionBodyParam)
        bundle.putSerializable(
            "doctorName",
            (activity as ActivityPatientDetailsUI).userData.doctorName
        )
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        intent.putExtras(bundle)

        //start video calling activity
        startActivity(intent)

        //pop back all the fragments
        Handler(Looper.getMainLooper()).postDelayed({
            //to re-initiate values
            setInitialValues()
            for (i in 0..((activity as ActivityPatientDetailsUI).supportFragmentManager.backStackEntryCount - 2)) {
                (activity as ActivityPatientDetailsUI).supportFragmentManager.popBackStack()
            }
        }, 500)
    }

    private fun setInitialValues() {
        (activity as ActivityPatientDetailsUI).dataEditingComplete = false
        //Resetting userData of ActivityPatientDetailsUI
        (activity as ActivityPatientDetailsUI).userData = UserData("")
        (activity as ActivityPatientDetailsUI).userData.bloodPressure = BloodPressure("", "")
        (activity as ActivityPatientDetailsUI).userData.vitals =
            Vitals((activity as ActivityPatientDetailsUI).userData.bloodPressure, "", "", "")
        (activity as ActivityPatientDetailsUI).userData.personal_details =
            PersonalDetails("", "", "")
        (activity as ActivityPatientDetailsUI).userData.getSessionBodyParam = GetSessionBodyParam(
            (activity as ActivityPatientDetailsUI).userData.personal_details,
            (activity as ActivityPatientDetailsUI).userData.vitals,
            (activity as ActivityPatientDetailsUI).userData.doctorName
        )
    }
}
