package com.vivek.activities.basicvideochat.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import com.vivek.activities.basicvideochat.model.AvailableDoctorList
import com.vivek.activities.basicvideochat.repository.GetAvailableDoctorListRepo
import com.vivek.activities.basicvideochat.utils.Resource

class GetAvailableDoctorListViewModel(application: Application) : AndroidViewModel(application) {
    private val uIMainActivityRepo: GetAvailableDoctorListRepo = GetAvailableDoctorListRepo()
    val availableDoctorListRepoLiveData: MediatorLiveData<Resource<AvailableDoctorList>> =
        MediatorLiveData<Resource<AvailableDoctorList>>()

    init {
        availableDoctorListRepoLiveData.addSource(uIMainActivityRepo.liveData){
            availableDoctorListRepoLiveData.value=it
        }
    }

    private val TAG = "VideoMainActivityViewModel"
    //TODO GetsessionMethod: will call actual method to call api from repository
    fun getAvailableDoctors() {
        Log.d(TAG,"inside getSession()")
        uIMainActivityRepo.getAvailableDoctors()
    }
}