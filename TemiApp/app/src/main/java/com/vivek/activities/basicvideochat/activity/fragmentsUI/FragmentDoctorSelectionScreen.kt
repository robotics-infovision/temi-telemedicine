package com.vivek.activities.basicvideochat.activity.fragmentsUI

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.activity.ActivityPatientDetailsUI
import com.vivek.activities.basicvideochat.activity.ActivityVideoCall
import com.vivek.activities.basicvideochat.adapter.AvailableDoctorListAdapter
import com.vivek.activities.basicvideochat.model.*
import com.vivek.activities.basicvideochat.my_interface.RecyclerViewClickListener
import com.vivek.activities.databinding.FragmentDoctorSelectionBinding


/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class FragmentDoctorSelectionScreen : Fragment(), RecyclerViewClickListener {
    private val hideHandler = Handler()

    lateinit var imageButtonNext: ImageButton
    var TAG: String = "DoctorSelectionScreen"
    lateinit var recyclerView: RecyclerView
    lateinit var adapterDoctorData: AvailableDoctorListAdapter
    private lateinit var availableDoctorDataThis: AvailableDoctorList
    lateinit var patientNameEditText: TextInputEditText
    var textInputWatcherName: TextInputLayout? = null
    var doctorSelectionScreenTag: String = "DoctorSelection"

    @Suppress("InlinedApi")
    private val hidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        val flags =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        activity?.window?.decorView?.systemUiVisibility = flags
        (activity as? AppCompatActivity)?.supportActionBar?.hide()
    }
    private val showPart2Runnable = Runnable {
        // Delayed display of UI elements
        fullscreenContentControls?.visibility = View.VISIBLE
    }
    private var visible: Boolean = false
//    private val hideRunnable = Runnable { hide() }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    @SuppressLint("ClickableViewAccessibility")
    private val delayHideTouchListener = View.OnTouchListener { _, _ ->
        if (AUTO_HIDE) {
//            delayedHide(AUTO_HIDE_DELAY_MILLIS)
        }
        false
    }

    private var fullscreenContent: View? = null
    private var fullscreenContentControls: View? = null
    private var _binding: FragmentDoctorSelectionBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        availableDoctorDataThis = (activity as ActivityPatientDetailsUI).availableDoctorData
        Log.i(TAG, "got the data: $availableDoctorDataThis")
        (ActivityPatientDetailsUI).selectedDoctorIndex =
            availableDoctorDataThis.data?.indexOfFirst {
                it.name_id == (activity as ActivityPatientDetailsUI).selectedDoctorId
            }
        (activity as ActivityPatientDetailsUI).selectedDoctorId?.let { Log.i(TAG, it) }
        Log.i("doctor index", (ActivityPatientDetailsUI).selectedDoctorIndex.toString())
        _binding = FragmentDoctorSelectionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI(_binding!!.root.findViewById<View>(R.id.doctorSelectionScreen))
        imageButtonNext = _binding!!.root.findViewById<View>(R.id.nextButton) as ImageButton
        recyclerView = _binding!!.root.findViewById<View>(R.id.doctorRecyclerView) as RecyclerView
        textInputWatcherName =
            _binding!!.root.findViewById<View>(R.id.text_input_layout) as TextInputLayout
        patientNameEditText =
            _binding!!.root.findViewById<View>(R.id.name_edit_text_input_edit_text) as TextInputEditText
        patientNameEditText.addTextChangedListener(vitalsText)

        // Create adapter passing in the sample user data
        // Create adapter passing in the sample user data
        Log.i(TAG, "got the data: $availableDoctorDataThis")
        adapterDoctorData =
            availableDoctorDataThis.data?.let { AvailableDoctorListAdapter(this, it) }!!
        // Attach the adapter to the recyclerview to populate items
        // Attach the adapter to the recyclerview to populate items
        recyclerView.apply {
            setHasFixedSize(true)
            adapter = adapterDoctorData
        }
        if ((ActivityPatientDetailsUI).selectedDoctorIndex!! >= 0) {
            (ActivityPatientDetailsUI).selectedDoctorIndex?.let { it ->
                recyclerView.smoothScrollToPosition(
                    it
                )
                (ActivityPatientDetailsUI).selectedDoctorIndex?.let { it1 ->
                    recyclerView.scrollToPosition(it1)
                }
            }
            for (i in 0..availableDoctorDataThis.data?.size!!) {
                if (i == (ActivityPatientDetailsUI).selectedDoctorIndex) {
                    i.let {
                        recyclerView.findViewHolderForLayoutPosition(
                            it
                        )
                    }?.itemView?.alpha = 0.75f
                    i.let {
                        recyclerView.findViewHolderForLayoutPosition(
                            it
                        )
                    }?.itemView?.background = (((activity?.applicationContext?.let {
                        androidx.core.content.ContextCompat.getDrawable(
                            it,
                            R.drawable.selected_doctor_background
                        )
                    })))
                    (activity as ActivityPatientDetailsUI).userData.doctorName =
                        (activity as ActivityPatientDetailsUI).selectedDoctorId.toString()
                    if ((activity as ActivityPatientDetailsUI).userData.personal_details.name.isNotEmpty()) {
                        imageButtonNext.isEnabled = true
                        imageButtonNext.isActivated = true
                        imageButtonNext.setImageDrawable(((this.context?.let {
                            androidx.core.content.ContextCompat.getDrawable(
                                it,
                                R.drawable.icon_next_enable
                            )
                        })))
                    }
                    Log.i(TAG, "selected")
                } else {
                    i.let {
                        recyclerView.findViewHolderForLayoutPosition(
                            it
                        )
                    }?.itemView?.alpha = 1.0f
                    i.let {
                        recyclerView.findViewHolderForLayoutPosition(
                            it
                        )
                    }?.itemView?.background = (((activity?.applicationContext?.let {
                        androidx.core.content.ContextCompat.getDrawable(
                            it,
                            R.drawable.unselected_doctor_background
                        )
                    })))
                }

            }
        }
        visible = true
        //temporarily made the button enabled even if no text is entered, change both to false
        //after implying hide keyboard method
        imageButtonNext.isEnabled = false
        imageButtonNext.isActivated = false
        imageButtonNext.setImageDrawable(((context?.let {
            androidx.core.content.ContextCompat.getDrawable(
                it,
                R.drawable.icon_next_disable
            )
        })))
        imageButtonNext.setOnClickListener {
            if ((patientNameEditText.text != null && patientNameEditText.text.toString() != "")) {
                Log.i(TAG, "next button clicked")
                if (imageButtonNext.isEnabled && imageButtonNext.isActivated) {
                    callNextFragment()
                }
            }
        }
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if ((activity as ActivityPatientDetailsUI).userData.doctorName == "trial") {
            imageButtonNext.isEnabled = false
            imageButtonNext.isActivated = false
            imageButtonNext.setImageDrawable(((context?.let {
                androidx.core.content.ContextCompat.getDrawable(
                    it,
                    R.drawable.icon_next_disable
                )
            })))
        }
        val inputMethodManager: InputMethodManager = activity.getSystemService(
            AppCompatActivity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(
                activity.currentFocus!!.windowToken,
                0
            )
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    fun setupUI(view: View) {
        // Set up touch listener for non-text box views to hide keyboard.
//        view !is EditText &&
        if (view !is LinearLayout) {
            view.setOnTouchListener { _, _ ->
                hideSoftKeyboard(activity as ActivityPatientDetailsUI)
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                setupUI(innerView)
            }
        }
    }


    private val vitalsText: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            //radio button for gender male female other
            //button will be enabled after all fields are filled
//            Log.i("character", s as String)
            // apply condition to check if doctor is selected or not
            imageButtonNext.isEnabled =
                patientNameEditText.text!!.isNotEmpty()
            imageButtonNext.isActivated = imageButtonNext.isEnabled

            if (imageButtonNext.isEnabled && patientNameEditText.text!!.isNotEmpty()) {
                imageButtonNext.setImageDrawable(((context?.let {
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.icon_next_enable
                    )
                })))
            } else {
                imageButtonNext.setImageDrawable((context?.let {
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.icon_next_disable
                    )
                }))
            }
            (activity as ActivityPatientDetailsUI).userData.personal_details.name =
                patientNameEditText.text.toString()
        }

        override fun afterTextChanged(s: Editable) {
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

    }

    override fun onPause() {
        super.onPause()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // Clear the systemUiVisibility flag
        activity?.window?.decorView?.systemUiVisibility = 0
//        show()
    }

    override fun onDestroy() {
        super.onDestroy()
        fullscreenContent = null
        fullscreenContentControls = null
    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private const val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private const val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private const val UI_ANIMATION_DELAY = 300
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun callNextFragment() {
        val fragment: FragmentGenderScreen = FragmentGenderScreen()
        val fragmentManager = requireActivity().supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.tempFragment, fragment, doctorSelectionScreenTag)
        fragmentTransaction.addToBackStack(doctorSelectionScreenTag)
        fragmentTransaction.commit()
    }

    override fun recyclerViewListClicked(v: View?, position: Int) {
        Log.i(TAG, "position is: $position")
    }

    //for fragments
    private fun establishVideoConference() {
        val intent: Intent =
            Intent(this.activity as ActivityPatientDetailsUI, ActivityVideoCall::class.java)

        val getSessionBodyParam: GetSessionBodyParam =
            GetSessionBodyParam(
                (activity as ActivityPatientDetailsUI).userData.personal_details,
                (activity as ActivityPatientDetailsUI).userData.vitals,
                (activity as ActivityPatientDetailsUI).userData.doctorName
            )
        val bundle = Bundle()
        bundle.putSerializable("data", getSessionBodyParam)
        bundle.putSerializable(
            "doctorName",
            (activity as ActivityPatientDetailsUI).userData.doctorName
        )
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        intent.putExtras(bundle)

        //start video calling activity
        startActivity(intent)

        //pop back all the fragments
        Handler(Looper.getMainLooper()).postDelayed({
            //to re-initiate values
            setInitialValues()
            for (i in 0..((activity as ActivityPatientDetailsUI).supportFragmentManager.backStackEntryCount - 2)) {
                (activity as ActivityPatientDetailsUI).supportFragmentManager.popBackStack()
            }
        }, 500)
    }

    private fun setInitialValues() {
        (activity as ActivityPatientDetailsUI).userData = UserData("")
        (activity as ActivityPatientDetailsUI).userData.bloodPressure = BloodPressure("", "")
        (activity as ActivityPatientDetailsUI).userData.vitals =
            Vitals((activity as ActivityPatientDetailsUI).userData.bloodPressure, "", "", "")
        (activity as ActivityPatientDetailsUI).userData.personal_details =
            PersonalDetails("", "", "")
        (activity as ActivityPatientDetailsUI).userData.getSessionBodyParam = GetSessionBodyParam(
            (activity as ActivityPatientDetailsUI).userData.personal_details,
            (activity as ActivityPatientDetailsUI).userData.vitals,
            (activity as ActivityPatientDetailsUI).userData.doctorName
        )
    }

}