package com.vivek.activities.basicvideochat.adapter

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.activity.ActivityPatientDetailsUI
import com.vivek.activities.basicvideochat.activity.fragmentsUI.FragmentDoctorSelectionScreen
import com.vivek.activities.basicvideochat.model.DoctorDataList
import com.vivek.activities.basicvideochat.my_interface.RecyclerViewClickListener
import de.hdodenhof.circleimageview.CircleImageView

class AvailableDoctorListAdapter(
    private val fragment: Fragment,
    private val dataSet: List<DoctorDataList>,
) :
    RecyclerView.Adapter<AvailableDoctorListAdapter.ViewHolder>(), View.OnClickListener {
    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */

    private var itemListener: RecyclerViewClickListener? = null
    private var onSessionListItemClick: ((DoctorDataList) -> Unit)? = null
    var itemSelectedPosition: Int = -1

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView = view.findViewById(R.id.doctorName)
        val imageView: CircleImageView = view.findViewById(R.id.doctorImage)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.doctordatalayout, viewGroup, false)
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        if(itemSelectedPosition==-1 && (ActivityPatientDetailsUI).selectedDoctorIndex!=null){
            itemSelectedPosition = (ActivityPatientDetailsUI).selectedDoctorIndex!!
        }
        if (itemSelectedPosition != -1 && itemSelectedPosition == position) {
            viewHolder.itemView.alpha = 1.0f
            viewHolder.itemView.background = (((fragment.activity?.applicationContext?.let {
                androidx.core.content.ContextCompat.getDrawable(
                    it,
                    R.drawable.selected_doctor_background
                )
            })))
        } else {
            viewHolder.itemView.alpha = 0.75f
            viewHolder.itemView.background = (((fragment.activity?.applicationContext?.let {
                androidx.core.content.ContextCompat.getDrawable(
                    it,
                    R.drawable.unselected_doctor_background
                )
            })))
        }

        viewHolder.textView.text = dataSet[position].name
        Glide.with(fragment)
            .asBitmap()
            .load(dataSet[position].img_url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    viewHolder.imageView.setImageBitmap(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                    // this is called when imageView is cleared on lifecycle call or for
                    // some other reason.
                    // if you are referencing the bitmap somewhere else too other than this imageView
                    // clear it here as you can no longer have the bitmap
                }
            })

        when (viewHolder) {
            else -> {
                viewHolder.itemView.setOnClickListener {

                    itemSelectedPosition = position
                    onSessionListItemClick?.invoke(dataSet[position])
                    (fragment.activity as ActivityPatientDetailsUI).userData.doctorName =
                        dataSet[position].name_id!!
                    if((fragment.activity as ActivityPatientDetailsUI).userData.personal_details.name.isNotEmpty()){
                        (fragment as FragmentDoctorSelectionScreen).imageButtonNext.isEnabled = true
                        (fragment as FragmentDoctorSelectionScreen).imageButtonNext.isActivated = true
                        (fragment as FragmentDoctorSelectionScreen).imageButtonNext.setImageDrawable(((fragment.context?.let {
                            androidx.core.content.ContextCompat.getDrawable(
                                it,
                                R.drawable.icon_next_enable
                            )
                        })))
                    }
                    Log.i("ItemClicked", viewHolder.textView.text.toString())
                    Log.i("item Clicked Position", viewHolder.layoutPosition.toString())
                    Log.i(
                        "From AvailableDoctorAdapter",
                        "Consult with: ${(fragment.activity as ActivityPatientDetailsUI).userData.doctorName}"
                    )
                    notifyDataSetChanged()
                }
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size
    override fun onClick(p0: View?) {

    }
}
