package com.vivek.activities.basicvideochat.model

import java.io.Serializable

data class GetSessionBodyParam(
    val personal_details: PersonalDetails,
    val vitals: Vitals,
    var doctorName: String
) : Serializable