package com.vivek.activities.basicvideochat.utils

class Constants {
    companion object{
        val blood_pressure_lower: Int = 50
        val blood_pressure_higher: Int = 200
        val weight_lower: Int = 2
        val weight_higher: Int = 250
        val age_lower: Int = 0
        val age_higher: Int = 150
        val pulse_lower: Int = 20
        val pulse_higher: Int = 200
        val temperature_lower: Float = 33.0F
        val temperature_higher: Float = 41.9F
        val spo2_lower: Int = 60
        val spo2_higher: Int = 100
    }
}