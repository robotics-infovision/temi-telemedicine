package com.vivek.activities.basicvideochat.repository

import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import com.vivek.activities.basicvideochat.model.GetSessionBodyParam
import com.vivek.activities.basicvideochat.model.ServerData
import com.vivek.activities.basicvideochat.my_interface.GetNoticeDataService
import com.vivek.activities.basicvideochat.network.RetrofitInstance
import com.vivek.activities.basicvideochat.utils.Resource
import com.vivek.activities.basicvideochat.utils.Status
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VideoMainActivityRepo : AppCompatActivity() {
    val liveData: MutableLiveData<Resource<ServerData>> =
        MutableLiveData<Resource<ServerData>>()

    val TAG = "VideoMainActivityRepo"
    fun getSession(requestBody: GetSessionBodyParam) {
        setObservableStatus(
            Status.LOADING,
            "Loading",
            null
        )
        Log.i(TAG, "inside getSession()")
        /** Create handle for the RetrofitInstance interface */
        val service: GetNoticeDataService? = RetrofitInstance.getRetrofitInstance()?.create(
            GetNoticeDataService::class.java
        )
        val url:String = "createSession?id="+requestBody.doctorName
        /** Call the method with parameter in the interface to get the notice data */
        val call: Call<ServerData>? = service?.createData(url,requestBody)
        /**Log the URL called */
        if (call != null) {
            Log.i("server data URL Called", call.request().url.toString() + "")
        }
        call?.enqueue(object : Callback<ServerData> {
            override fun onResponse(call: Call<ServerData>, response: Response<ServerData>) {
                Log.wtf(
                    TAG,
                    response.body()?.data?.apiKey
                )
                Log.wtf(
                    TAG,
                    response.body()?.data?.sessionId
                )
                Log.wtf(
                    TAG,
                    response.body()?.data?.token
                )
                Log.wtf(
                    TAG,
                    response.body()?.msg
                )

                //use live data to transfer the response data to activity via viewmodel
                if (response.isSuccessful) {
                    setObservableStatus(
                        Status.SUCCESS,
                        "message from Success: get session: onResponse: repo",
                        response.body()
                    )
                } else {
                    setObservableStatus(
                        Status.ERROR,
                        "message from Error: get session: onResponse: repo",
                        response.body()
                    )
                }
            }

            override fun onFailure(call: Call<ServerData>, t: Throwable) {
                setObservableStatus(
                    Status.ERROR,
                    "message from Error: get session: onFailure: repo",
                    response = null
                )
                Log.i("Something went wrong:" + t.message, Toast.LENGTH_SHORT.toString())
            }
        })
    }

    fun setObservableStatus(status: Status, message: String, response: ServerData?) {
        when (status) {
            Status.SUCCESS -> {
                liveData.value = Resource(Status.SUCCESS, response, message)
            }
            Status.ERROR -> {
                liveData.value = Resource(Status.ERROR, response, message)
            }
            Status.LOADING -> {
                liveData.value = Resource(Status.LOADING, response, message)
            }
        }
    }
}