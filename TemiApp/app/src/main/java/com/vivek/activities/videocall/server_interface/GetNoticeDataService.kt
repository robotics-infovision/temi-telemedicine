package com.vivek.activities.videocall.server_interface

import com.vivek.activities.videocall.model.ServerData
import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Query


interface GetNoticeDataService {
    /**
     * URL MANIPULATION
     * HTTP request body with the @Body annotation
     */
    @POST("createSession?id=vinay")
    fun createData(): Call<ServerData>

    @POST("createSession?")
    fun getData(@Query("id") doctor: String?): Call<ServerData>
}