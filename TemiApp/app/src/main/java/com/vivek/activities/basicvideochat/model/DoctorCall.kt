package com.vivek.activities.basicvideochat.model

import java.io.Serializable

data class DoctorCall(
    var doctorName:String
) : Serializable
