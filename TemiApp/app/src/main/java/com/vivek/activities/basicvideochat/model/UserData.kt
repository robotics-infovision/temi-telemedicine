package com.vivek.activities.basicvideochat.model

data class UserData(
    var doctorName: String = "",
){
    var bloodPressure: BloodPressure = BloodPressure("","")
    var vitals: Vitals = Vitals(bloodPressure,"","","","")
    var personal_details: PersonalDetails = PersonalDetails("","","")
    var getSessionBodyParam:GetSessionBodyParam = GetSessionBodyParam(personal_details,vitals,doctorName)
}
