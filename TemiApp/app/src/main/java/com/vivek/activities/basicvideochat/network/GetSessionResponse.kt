package com.vivek.activities.basicvideochat.network

import com.squareup.moshi.Json

class GetSessionResponse {
    @Json(name = "apiKey")
    var apiKey: String? = null

    @Json(name = "sessionId")
    var sessionId: String? = null

    @Json(name = "token")
    var token: String? = null
}