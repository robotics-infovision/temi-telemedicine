package com.vivek.activities.basicvideochat.activity


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.aigestudio.wheelpicker.WheelPicker
import com.google.android.material.snackbar.Snackbar
import com.robotemi.sdk.Robot
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.activity.fragmentsUI.FragmentWelcomeScreen
import com.vivek.activities.basicvideochat.model.*
import com.vivek.activities.basicvideochat.my_interface.APIService
import com.vivek.activities.basicvideochat.network.ServerConfig
import com.vivek.activities.basicvideochat.utils.Status
import com.vivek.activities.basicvideochat.viewmodel.EmailViewModel
import com.vivek.activities.basicvideochat.viewmodel.GetAvailableDoctorListViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


class ActivityPatientDetailsUI : AppCompatActivity(R.layout.ac_preview),
    WheelPicker.OnItemSelectedListener,
    View.OnClickListener {
    private lateinit var viewModelDoctorList: GetAvailableDoctorListViewModel
    lateinit var viewModelEMail: EmailViewModel
    private var retrofit: Retrofit? = null
    private var apiService: APIService? = null
    var availableDoctorData: AvailableDoctorList = AvailableDoctorList()
    var patientEmail: EmailRequest? = EmailRequest()
    var patientEmailResponse: EmailResponse = EmailResponse()
    var emailSendStatus: String? = null
    val TAG: String = "WheelChooseActivity"
    var activityScreenTag: String = "Activity"
    var userData: UserData = UserData("")
    var dataEditingComplete: Boolean = false
    var selectedDoctorId: String? = null
    var snackBar: Snackbar? = null

    lateinit var robot: Robot
    override fun onCreate(savedInstanceState: Bundle?) {
        val bundle: Bundle? = intent.extras
        if (bundle != null) {
            selectedDoctorId = bundle.getString("doctorId")
            selectedDoctorId?.let { Log.i(TAG, it) }
        }
        when (intent?.action) {
            Intent.ACTION_SEND -> {
                if (intent.type == "text/plain") {
                    handleSendText(intent) // Handle text being sent
                }
            }
        }
        robot = Robot.getInstance()
        robot.hideTopBar()
        viewModelDoctorList =
            ViewModelProvider(this).get(GetAvailableDoctorListViewModel::class.java)
        viewModelEMail = ViewModelProvider(this).get(EmailViewModel::class.java)
        initRetrofit()
        getDoctorData()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ac_preview)

        if (savedInstanceState == null) {
            val bundle = Bundle()
            bundle.putSerializable("doctorData", availableDoctorData)
            val fragment = FragmentWelcomeScreen()
            fragment.arguments = bundle
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.tempFragment, fragment, activityScreenTag)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
    }

    private fun handleSendText(intent: Intent) {
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            selectedDoctorId = it
        }
        selectedDoctorId?.let { Log.i(TAG, it) }
    }

    override fun onItemSelected(picker: WheelPicker, data: Any, position: Int) {
    }

    override fun onClick(v: View?) {
    }

    override fun onStart() {
        super.onStart()
        initObservers()
    }

    private fun initRetrofit() {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
        retrofit = ServerConfig.CHAT_SERVER_URL?.let {
            Retrofit.Builder()
                .baseUrl(it)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(client)
                .build()
        }
        apiService = retrofit!!.create(APIService::class.java)
    }

    private fun initObservers() {
        viewModelDoctorList.availableDoctorListRepoLiveData.observe(this@ActivityPatientDetailsUI) {
            when (it.status) {
                Status.SUCCESS -> {
                    availableDoctorData.msg = it.data?.msg
                    availableDoctorData.data = it.data?.data
                }
                Status.ERROR -> {
                    snackBar = Snackbar.make(
                        findViewById(R.id.mainActivity),
                        "Could not fetch available doctors.",
                        Snackbar.LENGTH_INDEFINITE
                    ).setAction(
                        "Retry",
                        View.OnClickListener {
                            getDoctorData()
                            snackBar?.dismiss()
                        }
                    )
                    snackBar!!.show()
                    availableDoctorData.msg = "trial_msg"
                    availableDoctorData.data = listOf(
                        DoctorDataList(
                            "trial_id1",
                            "trial_name_id1",
                            "trial_name1",
                            "trial_image_url1"
                        ),
                        DoctorDataList(
                            "trial_id2",
                            "trial_name_id2",
                            "trial_name2",
                            "trial_image_url2"
                        ),
                        DoctorDataList(
                            "trial_id3",
                            "trial_name_id3",
                            "trial_name3",
                            "trial_image_url3"
                        )
                    )                    //finish with message with message please try again
                }
                Status.LOADING -> {
                    Log.d(TAG, "Status: LOADING")
                    availableDoctorData.msg = "trial_msg"
                    availableDoctorData.data = listOf(
                        DoctorDataList(
                            "trial_id1",
                            "trial_name_id1",
                            "trial_name1",
                            "trial_image_url1"
                        ),
                        DoctorDataList(
                            "trial_id2",
                            "trial_name_id2",
                            "trial_name2",
                            "trial_image_url2"
                        ),
                        DoctorDataList(
                            "trial_id3",
                            "trial_name_id3",
                            "trial_name3",
                            "trial_image_url3"
                        )
                    )
                }
            }
        }
    }

    private fun initObserversEmail() {
        viewModelEMail.emailRepoLiveData.observe(this@ActivityPatientDetailsUI) {
            when (it.status) {
                Status.SUCCESS -> {
                    patientEmailResponse.setMsg(it.data?.msg.toString())
                    patientEmailResponse.setData(it.data?.data!!)
                    Log.i(Status.SUCCESS.toString() + ": from MainActivity", it.data.msg)
                    Log.i(
                        Status.SUCCESS.toString() + ": from MainActivity",
                        patientEmailResponse.getMsg()
                    )
                    showEmailSentToast()
                    emailSendStatus = "SUCCESS"
                }
                Status.ERROR -> {
                    patientEmailResponse.msg = it.data?.msg.toString()
                    patientEmailResponse.data = it.data?.data!!
                    Log.i(Status.ERROR.toString() + ": from MainActivity", it.data.msg)
                    showEmailNotSentToast()
                    emailSendStatus = "ERROR"
                }
                Status.LOADING -> {
                    patientEmailResponse.msg = it.data?.msg.toString()
                    patientEmailResponse.data = it.data?.data!!
                    Log.i(Status.LOADING.toString() + ": from MainActivity", it.data.msg)
                    showEmailNotSentToast()
                    emailSendStatus = "LOADING"
                }
            }
        }
    }

    private fun getDoctorData() {
        Log.i(TAG, "inside getDoctorData")
        //call createsession method from viewmodel
        initObserversEmail()
        viewModelDoctorList.getAvailableDoctors()
    }

    fun sendPatientEmail() {
        Log.i(TAG, "Sending request to server for email update")
        this.patientEmail?.let { viewModelEMail.sendPatientEmail(it) }
    }

    override fun onBackPressed() {
        super.onBackPressed()

//        supportFragmentManager.popBackStack()
    }

    fun goToNavigatorApp() {
//        val intent = Intent()
//        intent.component = ComponentName(
//            "com.temi.navigation",
//            "com.temi.navigation.activity.MainActivity"
//        )
//        if (intent.resolveActivity(packageManager) != null) {
//            startActivity(intent)
//        }
        finish()
    }

    private fun showEmailSentToast() {
        Snackbar.make(
            findViewById(R.id.mainActivity),
            patientEmailResponse.getMsg(),
            Snackbar.LENGTH_LONG
        ).show()
    }

    private fun showEmailNotSentToast() {
        var snackBarEmailNotSent: Snackbar? = null
        snackBarEmailNotSent = Snackbar.make(
            findViewById(R.id.mainActivity),
            "Email updation failed, please try again.",
            Snackbar.LENGTH_INDEFINITE
        ).setAction(
            "Retry",
            View.OnClickListener {
                sendPatientEmail()
                snackBarEmailNotSent?.dismiss()
            }
        )
        snackBarEmailNotSent.show()
    }

    companion object {
        var callCompleted: Boolean = false
        var callCompletedNumber: Int = 0
        var selectedDoctorIndex: Int? = null
    }
}