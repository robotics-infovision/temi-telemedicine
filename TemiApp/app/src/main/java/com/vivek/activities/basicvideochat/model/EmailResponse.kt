package com.vivek.activities.basicvideochat.model

class EmailResponse{
    var msg:String = ""
    var data:Data = Data("")

    @JvmName("setMsg1")
    fun setMsg(msg:String){
        this.msg=msg
    }
    @JvmName("getMsg1")
    fun getMsg(): String {
        return this.msg
    }
    @JvmName("setData1")
    fun setData(data:Data){
        this.data=data
    }
    @JvmName("getData1")
    fun getData(): Data {
        return this.data
    }
}