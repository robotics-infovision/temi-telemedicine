package com.vivek.activities.temi.trial

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.robotemi.sdk.Robot
import com.robotemi.sdk.listeners.OnConversationStatusChangedListener
import com.robotemi.sdk.listeners.OnDetectionDataChangedListener
import com.robotemi.sdk.listeners.OnDetectionStateChangedListener
import com.robotemi.sdk.listeners.OnUserInteractionChangedListener
import com.robotemi.sdk.model.DetectionData
import com.vivek.activities.R

class UIMainActivity : AppCompatActivity(), OnDetectionStateChangedListener,
    OnUserInteractionChangedListener, OnDetectionDataChangedListener,
    OnConversationStatusChangedListener, Robot.AsrListener {
    var robot: Robot? = null
    var button: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.trial_temi_telemedicine)
        button = findViewById(R.id.btn_call_doctor)

        //robot functions
        robot = Robot.getInstance()
        robot!!.setKioskModeOn(false)
        button!!.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@UIMainActivity, com.vivek.activities.videocall.activity.VideoMainActivity::class.java)
            startActivity(intent)
        })
    }

    public override fun onStart() {
        super.onStart()
    }

    public override fun onStop() {
        super.onStop()
    }

    override fun onDetectionStateChanged(i: Int) {}
    override fun onUserInteraction(b: Boolean) {}
    override fun onDetectionDataChanged(detectionData: DetectionData) {}
    override fun onAsrResult(s: String) {}
    override fun onConversationStatusChanged(i: Int, s: String) {}
}