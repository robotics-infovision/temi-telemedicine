package com.vivek.activities.basicvideochat.model

import java.io.Serializable

data class Data(
    var id: String = ""
) : Serializable
