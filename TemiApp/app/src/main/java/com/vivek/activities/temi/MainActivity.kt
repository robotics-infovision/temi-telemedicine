package com.vivek.activities.temi.trial

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.robotemi.sdk.Robot
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.activity.ActivityVideoCall

class MainActivity : AppCompatActivity() {
    var robot: Robot? = null
    var button: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.temi_telemedicine)
        button = findViewById<View>(R.id.btn_call_doctor) as Button
        robot = Robot.getInstance()
        button!!.setOnClickListener { v: View? ->
            val intent = Intent(this@MainActivity, ActivityVideoCall::class.java)
            startActivity(intent)
        }
        val buttonclick = findViewById<Button>(R.id.btn_call_doctor)
        buttonclick.setOnClickListener { v: View? ->
            val intent = Intent(this@MainActivity, ActivityVideoCall::class.java)
            startActivity(intent)
        }
    }
}