package com.vivek.activities.basicvideochat.model

import java.io.Serializable

data class DoctorDataList(
    var id:String?=null,
    var name_id:String?=null,
    var name:String?=null,
    var img_url:String?=null
):Serializable
