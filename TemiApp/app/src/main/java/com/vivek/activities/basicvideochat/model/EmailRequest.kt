package com.vivek.activities.basicvideochat.model

import java.io.Serializable

data class EmailRequest(
    var sessionId: String = "",
    var doctor_id: String = "",
    var email: String = ""
) : Serializable
