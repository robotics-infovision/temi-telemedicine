package com.vivek.activities.basicvideochat.model

import java.io.Serializable

data class AvailableDoctorList(
    var msg:String?=null,
var data:List<DoctorDataList>?=null
):Serializable
