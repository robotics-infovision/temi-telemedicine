package com.vivek.activities.basicvideochat.my_interface

import android.view.View

interface RecyclerViewClickListener {
    fun recyclerViewListClicked(v: View?, position: Int)
}