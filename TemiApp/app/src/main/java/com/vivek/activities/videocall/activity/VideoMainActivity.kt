package com.vivek.activities.videocall.activity


import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.opentok.android.*
import com.opentok.android.PublisherKit.PublisherListener
import com.opentok.android.Session.SessionListener
import com.opentok.android.SubscriberKit.SubscriberListener
import com.robotemi.sdk.Robot
import com.robotemi.sdk.TtsRequest
import com.vivek.activities.R
import com.vivek.activities.temi.trial.UIMainActivity
import com.vivek.activities.videocall.activity.VideoMainActivity
import com.vivek.activities.videocall.model.DataSub
import com.vivek.activities.videocall.model.ServerData
import com.vivek.activities.videocall.network.OpenTokConfig
import com.vivek.activities.videocall.network.RetrofitInstance
import com.vivek.activities.videocall.network.ServerConfig
import com.vivek.activities.videocall.server_interface.APIService
import com.vivek.activities.videocall.server_interface.GetNoticeDataService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.EasyPermissions.PermissionCallbacks
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*


class VideoMainActivity() : AppCompatActivity(),
    PermissionCallbacks {
    var robot: Robot = Robot.getInstance()
    private var retrofit: Retrofit? = null
    private var apiService: APIService? = null
    private var session: Session? = null
    var publisher: Publisher? = null
    private var subscriber: Subscriber? = null
    private var publisherViewContainer: FrameLayout? = null
    private var subscriberViewContainer: FrameLayout? = null
    var flMainViewContainer: FrameLayout? = null
    var ChooseButton: FrameLayout? = null
    var ControlButtons: LinearLayout? = null
    var temiSpeak: FrameLayout? = null
    var temiGreetTime: TextView? = null
    private val PLAYER_CONTROLS_VISIBILITY_TIMEOUT = 1000
    var controlHandler = Handler()
    var muted = false
    private val publisherListener: PublisherListener = object : PublisherListener {
        override fun onStreamCreated(publisherKit: PublisherKit, stream: Stream) {
            Log.d(
                VIDEO_TAG,
                "onStreamCreated: Publisher Stream Created. Own stream " + stream.streamId
            )
        }

        override fun onStreamDestroyed(publisherKit: PublisherKit, stream: Stream) {
            Log.d(
                VIDEO_TAG,
                "onStreamDestroyed: Publisher Stream Destroyed. Own stream " + stream.streamId
            )
            disconnectSession()
        }

        override fun onError(publisherKit: PublisherKit, opentokError: OpentokError) {
            finishWithMessage(getString(R.string.call_disconnected))
            disconnectSession()
            Log.d(
                VIDEO_TAG,
                "onError: publisherListener Error= " + opentokError.message + " in session: " + session!!.sessionId
            )
        }
    }
    val sessionListener: SessionListener = object : SessionListener {
        override fun onConnected(session: Session) {
            Log.d(VIDEO_TAG, "onConnected: Connected to session: " + session.sessionId)
            publisher = Publisher.Builder(this@VideoMainActivity).build()
            publisher?.setPublisherListener(publisherListener)
            publisher?.renderer
                ?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL)
            publisherViewContainer!!.addView(publisher?.getView())
            if (publisher?.getView() is GLSurfaceView) {
                (publisher?.getView() as GLSurfaceView).setZOrderOnTop(true)
            }
            session.publish(publisher)
        }

        override fun onDisconnected(session: Session) {
            Log.d(VIDEO_TAG, "onDisconnected: Disconnected from session: " + session.sessionId)
            disconnectSession()
        }

        override fun onStreamReceived(session: Session, stream: Stream) {
            Log.d(
                VIDEO_TAG,
                "onStreamReceived: New Stream Received " + stream.streamId + " in session: " + session.sessionId
            )
            temiSpeak!!.visibility = View.GONE
            publisherViewContainer!!.visibility = View.VISIBLE
            if (subscriber == null) {
                subscriber = Subscriber.Builder(this@VideoMainActivity, stream).build()
                subscriber?.renderer?.setStyle(
                    BaseVideoRenderer.STYLE_VIDEO_SCALE,
                    BaseVideoRenderer.STYLE_VIDEO_FILL
                )
                subscriber?.setSubscriberListener(subscriberListener)
                session.subscribe(subscriber)
                subscriberViewContainer!!.addView(subscriber?.getView())
            }
        }

        override fun onStreamDropped(session: Session, stream: Stream) {
            Log.d(
                VIDEO_TAG,
                "onStreamDropped: Stream Dropped: " + stream.streamId + " in session: " + session.sessionId
            )
            disconnectSession()
        }

        override fun onError(session: Session, opentokError: OpentokError) {
            Log.d(
                VIDEO_TAG,
                "sessionListener onError: Error= " + opentokError.message + " in session: " + session.sessionId
            )
            finishWithMessage(getString(R.string.call_disconnected)) // coming when call is disconnected
            //disconnectSession();
        }
    }
    var subscriberListener: SubscriberListener = object : SubscriberListener {
        override fun onConnected(subscriberKit: SubscriberKit) {
            Log.d(
                VIDEO_TAG,
                "onConnected: Subscriber connected. Stream: " + subscriberKit.stream.streamId
            )
        }

        override fun onDisconnected(subscriberKit: SubscriberKit) {
            Log.d(
                VIDEO_TAG,
                "onDisconnected: Subscriber disconnected. Stream: " + subscriberKit.stream.streamId
            )
            disconnectSession()
        }

        override fun onError(subscriberKit: SubscriberKit, opentokError: OpentokError) {
            disconnectSession()
            finishWithMessage("SubscriberKit onError: " + opentokError.message)
            Log.d(
                VIDEO_TAG,
                "onError: subscriberListener Error= " + opentokError.message + " in session: " + session!!.sessionId
            )
        }
    }
    private var pausedAudio = false
    fun buttonPressedMute(view: View) {
        val button = view as ImageButton
        val icon: Int
        if (pausedAudio) {
            pausedAudio = false
            icon = R.drawable.ic_unmuted
            getDrawable(R.drawable.round_green).also { button.background = it }
        } else {
            pausedAudio = true
            icon = R.drawable.ic_muted
            getDrawable(R.drawable.round_grey).also { button.background = it }
        }
        button.setImageDrawable(ContextCompat.getDrawable(applicationContext, icon))
    }

    private var pausedVideo = true
    fun buttonPressedVideo(view: View) {
        val button = view as ImageButton
        val icon: Int
        if (pausedVideo) {
            pausedVideo = false
            icon = R.drawable.video_cam_on_trial
        } else {
            pausedVideo = true
            icon = R.drawable.video_cam_off_trial
        }
        button.setImageDrawable(ContextCompat.getDrawable(applicationContext, icon))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.video_activity_main)
        temiGreetTime = findViewById<TextView>(R.id.temi_second_sentence)
        addFullStop()
        temiGreet()
        robot=Robot.getInstance()

        //FrameLayout for text of what tem iis speaking until stream is getting connected
        temiSpeak = findViewById(R.id.temi_speak_wait_frame)
        temiSpeak?.visibility = View.VISIBLE
        temiSpeak?.bringToFront()

        //Temi Speaks the text provided
        val ttsRequest = TtsRequest.create(
            temiGreetTime?.text.toString() + ". " +
                    getString(R.string.temi_speak_line_3) + ". " +
                    getString(R.string.temi_speak_line_4) + ".",
            false, TtsRequest.Language.EN_US
        )
        robot.speak(ttsRequest)
        robot.setKioskModeOn(true)

        //Containers
        flMainViewContainer = findViewById(R.id.main_container)
        ChooseButton = findViewById(R.id.toggle_button)
        ControlButtons = findViewById(R.id.controlBottons)
        publisherViewContainer = findViewById(R.id.publisher_container)
        publisherViewContainer?.visibility = View.GONE
        subscriberViewContainer = findViewById(R.id.subscriber_container)
        requestPermissions()
        controlButtonsAction()
        val muteunmute = findViewById<ImageButton>(R.id.btn_mute_call)
        val cutcall = findViewById<ImageButton>(R.id.btn_call_end)

        //To mute the stream
        muteunmute.setOnClickListener { view: View ->
            buttonPressedMute(view)
            publisher!!.publishAudio = muted
            if (muted) {
                Toast.makeText(this@VideoMainActivity, getString(R.string.unmuted), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this@VideoMainActivity, getString(R.string.muted), Toast.LENGTH_SHORT).show()
            }
            muted = !muted
        }

        //To cut the call
        cutcall.setOnClickListener {
            sessionListener.onDisconnected(session)
            sessionListener.onStreamDropped(session, subscriber!!.stream)
            val intent: Intent = Intent(this, UIMainActivity::class.java)
            startActivity(intent)
        }
        Log.i("Hello " + robot.isKioskModeOn(), Toast.LENGTH_SHORT.toString())
    }

    fun temiGreet() {
        //What to greet (good morning etc)
        val c = Calendar.getInstance()
        val timeOfDay = c[Calendar.HOUR_OF_DAY]
        if (timeOfDay < 12) {
            temiGreetTime?.text = getString(R.string.greet_good_morning)
        } else if (timeOfDay in 12..15) {
            temiGreetTime?.text = getString(R.string.greet_good_afternoon)
        } else if (timeOfDay in 16..23) {
            temiGreetTime?.text = getString(R.string.greet_good_evening)
        }
    }

    @SuppressLint("SetTextI18n")
    fun addFullStop() {
//        val sentence2:TextView? = findViewById<TextView>(R.id.temi_second_sentence)
//        sentence2?.text = getString(R.string.temi_speak_line_2).toString() + "."
//        val sentence3:TextView? = findViewById<TextView>(R.id.temi_third_sentence)
//        sentence3?.text = getString(R.string.temi_speak_line_3).toString() + "."
//        val sentence4:TextView? = findViewById<TextView>(R.id.temi_fourth_sentence)
//        sentence4?.text = getString(R.string.temi_speak_line_4).toString() + "."
//        val sentence5:TextView? = findViewById<TextView>(R.id.temi_fifth_sentence)
//        sentence5?.text = getString(R.string.temi_speak_line_5).toString() + "."
    }

    @SuppressLint("ClickableViewAccessibility")
    fun controlButtonsAction() {
        flMainViewContainer!!.setOnTouchListener { _: View?, _: MotionEvent? ->
            animate(0)
            ChooseButton!!.animate().translationY(0f)
            controlHandler.removeCallbacks(hidePlayerControls)
            controlHandler.postDelayed(
                hidePlayerControls,
                PLAYER_CONTROLS_VISIBILITY_TIMEOUT.toLong()
            )
            false
        }
    }

    private fun animate(height: Int) {
        for (i in 0..ControlButtons!!.childCount) {
            val animate = ControlButtons!!.getChildAt(i).animate()
            animate.startDelay = (i * 100).toLong()
            animate.translationY(height.toFloat())
        }
    }

    private val hidePlayerControls = Runnable {
        animate(
            ControlButtons!!.height
        )
    }

    fun disconnectSession() {
        if (session == null) {
            return
        }
        if (subscriber!!.stream != null) {
            session!!.unsubscribe(subscriber)
            subscriber!!.destroy()
        }
        if (publisher != null) {
            publisherViewContainer!!.removeView(publisher!!.view)
            session!!.unpublish(publisher)
            publisher!!.destroy()
            publisher = null
        }
        session!!.disconnect()
        finish() //after pop up
    }

    override fun onPause() {
        super.onPause()
        if (session != null) {
            session!!.onPause()
        }
    }

    override fun onResume() {
        super.onResume()
        if (session != null) {
            session!!.onResume()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        Log.d(
            TAG,
            "onPermissionsGranted:$requestCode: $perms"
        )
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        finishWithMessage("onPermissionsDenied: $requestCode: $perms")
    }

    @AfterPermissionGranted(PERMISSIONS_REQUEST_CODE)
    private fun requestPermissions() {
        val perms = arrayOf(
            Manifest.permission.INTERNET,
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
        )
        if (EasyPermissions.hasPermissions(this, *perms)) {
            if (ServerConfig.hasChatServerUrl()) {
                // Custom server URL exists - retrieve session config
                if (!ServerConfig.isValid) {
                    finishWithMessage("Invalid chat server url: " + ServerConfig.CHAT_SERVER_URL)
                    return
                }
                initRetrofit()
                getSession()
            } else {
                // Use hardcoded session config
                if (!OpenTokConfig.isValid) {
                    finishWithMessage("Invalid OpenTokConfig. " + OpenTokConfig.getDescription())
                    return
                }
                initializeSession(
                    OpenTokConfig.API_KEY,
                    OpenTokConfig.SESSION_ID,
                    OpenTokConfig.TOKEN
                )
            }
        } else {
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.rationale_video_app),
                PERMISSIONS_REQUEST_CODE,
                *perms
            )
        }
    }

    /* Make a request for session data */
    private fun getSession() {
        Log.i(TAG, "getSession")
        /** Create handle for the RetrofitInstance interface */
        val service: GetNoticeDataService? = RetrofitInstance.getRetrofitInstance()?.create(
            GetNoticeDataService::class.java)

        /** Call the method with parameter in the interface to get the notice data */
        val call: Call<ServerData>? = service?.createData()
        /**Log the URL called */
        if (call != null) {
            Log.i("server data URL Called", call.request().url.toString() + "")
        }
        call?.enqueue(object : Callback<ServerData> {
            override fun onResponse(call: Call<ServerData>, response: Response<ServerData>) {
                Log.wtf(
                    getString(R.string.log_api_on_response),
                    response.body()?.data?.apiKey
                )
                Log.wtf(
                    getString(R.string.log_session_on_response),
                    response.body()?.data?.sessionId
                )
                Log.wtf(
                    getString(R.string.log_token_on_response),
                    response.body()?.data?.token
                )
                Log.wtf(
                    getString(R.string.log_message_on_response),
                    response.body()?.msg
                )
                data = response.body()?.data!!
                initializeSession(data.apiKey.toString(), data.sessionId.toString(), data.token.toString())
            }

            override fun onFailure(call: Call<ServerData>, t: Throwable) {
                Log.i("Something went wrong:" + t.message, Toast.LENGTH_SHORT.toString())
                Toast.makeText(
                    this@VideoMainActivity,
                    "Something went wrong...Error message: " + t.message,
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    private fun initializeSession(apiKey: String, sessionId: String, token: String) {
        Log.i(TAG, getString(R.string.log_api_initialize_session) + apiKey)
        Log.i(TAG, getString(R.string.log_session_id_initialize_session) + sessionId)
        Log.i(TAG, getString(R.string.log_token_initialize_session) + token)

        /*
        The context used depends on the specific use case, but usually, it is desired for the session to
        live outside of the Activity e.g: live between activities. For a production applications,
        it's convenient to use Application context instead of Activity context.
         */
        session = Session.Builder(this, apiKey, sessionId).build()
        session?.setSessionListener(sessionListener)
        session?.connect(token)
    }

    private fun initRetrofit() {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
        retrofit = Retrofit.Builder()
            .baseUrl(ServerConfig.CHAT_SERVER_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .client(client)
            .build()
        apiService = retrofit!!.create(APIService::class.java)
    }

    private fun finishWithMessage(message: String) {
        Log.e(TAG, message)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        finish()
    }

    public override fun onStart() {
        super.onStart()
    }

    public override fun onStop() {
        super.onStop()
    }

    companion object {
        val TAG = VideoMainActivity::class.java.simpleName
        val VIDEO_TAG = "VIDEO_TAG"
        var data: DataSub = DataSub()
        const val PERMISSIONS_REQUEST_CODE = 124
    }
}
