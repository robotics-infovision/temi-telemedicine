package com.vivek.activities.basicvideochat.model

import java.io.Serializable

data class BloodPressure (
    var upper: String = "",
    var lower: String = "",
) : Serializable
