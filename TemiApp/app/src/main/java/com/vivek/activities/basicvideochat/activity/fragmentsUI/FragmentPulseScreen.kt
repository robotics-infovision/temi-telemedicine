package com.vivek.activities.basicvideochat.activity.fragmentsUI

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.aigestudio.wheelpicker.WheelPicker
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.activity.ActivityPatientDetailsUI
import com.vivek.activities.basicvideochat.activity.ActivityVideoCall
import com.vivek.activities.basicvideochat.model.*
import com.vivek.activities.databinding.FragmentPulseScreenBinding

/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class FragmentPulseScreen : Fragment(), WheelPicker.OnItemSelectedListener, View.OnClickListener {
    private val hideHandler = Handler()

    private var wheelLeft: WheelPicker? = null
    val bp1: MutableList<Int> = mutableListOf()
    var databp1: List<Int>? = null;
    var imageButtonNext: ImageButton? = null
    var imageButtonBack: ImageButton? = null
    private var gotoBtnItemIndex: Int? = null
    public var TAG: String = "PulseScreen"
    var pulseScreenTag: String = "Pulse"
    var consultButton: Button? = null
    var summaryButton: Button? = null


    @Suppress("InlinedApi")
    private val hidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        val flags =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        activity?.window?.decorView?.systemUiVisibility = flags
        (activity as? AppCompatActivity)?.supportActionBar?.hide()
    }
    private val showPart2Runnable = Runnable {
        // Delayed display of UI elements
        fullscreenContentControls?.visibility = View.VISIBLE
    }
    private var visible: Boolean = false
//    private val hideRunnable = Runnable { hide() }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    @SuppressLint("ClickableViewAccessibility")
    private val delayHideTouchListener = View.OnTouchListener { _, _ ->
        if (AUTO_HIDE) {
//            delayedHide(AUTO_HIDE_DELAY_MILLIS)
        }
        false
    }

    private var fullscreenContent: View? = null
    private var fullscreenContentControls: View? = null

    private var _binding: FragmentPulseScreenBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentPulseScreenBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        visible = true
        allFunctionsOnCreated()
    }

    private fun allFunctionsOnCreated() {
        setVisiblityConsultationButton()
        setFindViewById()
        setCustomizedData()
        setOnClickListeners()
    }

    private fun setFindViewById() {
        wheelLeft = _binding!!.root.findViewById<View>(R.id.main_wheel_left) as WheelPicker
        imageButtonNext = _binding!!.root.findViewById<View>(R.id.nextButton) as ImageButton?
        imageButtonBack = _binding!!.root.findViewById<View>(R.id.backButton) as ImageButton?
    }

    private fun setCustomizedData() {
        for (i in 30..200) {
            bp1.add(i - 30, i)
        }
        databp1 = bp1
        wheelLeft?.data = databp1
        wheelLeft?.selectedItemPosition = 42
        wheelLeft?.setSelectedItemPosition(42, true)
        imageButtonNext?.isActivated = false
        imageButtonNext?.isEnabled = false
        imageButtonNext?.setImageDrawable(((context?.let {
            androidx.core.content.ContextCompat.getDrawable(
                it,
                R.drawable.icon_next_disable
            )
        })))
    }

    fun setOnClickListeners() {
        wheelLeft?.setOnItemSelectedListener(this)
        imageButtonNext?.setOnClickListener {
            if (imageButtonNext?.isActivated == true &&
                imageButtonNext?.isEnabled == true
            ) {
                Log.i(TAG, "next button clicked")
                callNextFragment()
            }
        }
        imageButtonBack?.setOnClickListener {
            Log.i(TAG, "back button clicked")
            callPreviousFragment()
        }
        if ((activity as ActivityPatientDetailsUI).dataEditingComplete) {
            setCurrentState()
        }
    }

    private fun setVisiblityConsultationButton() {
        if (!(activity as ActivityPatientDetailsUI).dataEditingComplete) {
            (_binding!!.root.findViewById<View>(R.id.consultButton) as Button).visibility =
                View.GONE
            (_binding!!.root.findViewById<View>(R.id.backToSummary) as Button).visibility =
                View.GONE
        } else {
            (_binding!!.root.findViewById<View>(R.id.nextButton) as ImageButton).visibility =
                View.GONE
            consultButton = _binding!!.root.findViewById<View>(R.id.consultButton) as Button
            summaryButton = _binding!!.root.findViewById<View>(R.id.backToSummary) as Button
            consultButton?.setOnClickListener {
                    establishVideoConference()
            }
            summaryButton?.setOnClickListener {
                callNextFragment()
            }
        }
    }

    private fun setCurrentState() {
        wheelLeft?.selectedItemPosition =
            Integer.parseInt((activity as ActivityPatientDetailsUI).userData.vitals.pulse) - 30
        wheelLeft?.setSelectedItemPosition(
            Integer.parseInt((activity as ActivityPatientDetailsUI).userData.vitals.pulse) - 30,
            true
        )
    }

    override fun onResume() {
        super.onResume()
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
//        delayedHide(100)
    }

    override fun onPause() {
        super.onPause()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // Clear the systemUiVisibility flag
        activity?.window?.decorView?.systemUiVisibility = 0
//        show()
    }

    override fun onDestroy() {
        super.onDestroy()
        fullscreenContent = null
        fullscreenContentControls = null
    }

//    private fun toggle() {
//        if (visible) {
//            hide()
//        } else {
//            show()
//        }
//    }
//
//    private fun hide() {
//        // Hide UI first
//        fullscreenContentControls?.visibility = View.GONE
//        visible = false
//
//        // Schedule a runnable to remove the status and navigation bar after a delay
//        hideHandler.removeCallbacks(showPart2Runnable)
//        hideHandler.postDelayed(hidePart2Runnable, UI_ANIMATION_DELAY.toLong())
//    }
//
//    @Suppress("InlinedApi")
//    private fun show() {
//        // Show the system bar
//        fullscreenContent?.systemUiVisibility =
//            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
//                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//        visible = true
//
//        // Schedule a runnable to display UI elements after a delay
//        hideHandler.removeCallbacks(hidePart2Runnable)
//        hideHandler.postDelayed(showPart2Runnable, UI_ANIMATION_DELAY.toLong())
//        (activity as? AppCompatActivity)?.supportActionBar?.show()
//    }
//
//    /**
//     * Schedules a call to hide() in [delayMillis], canceling any
//     * previously scheduled calls.
//     */
//    private fun delayedHide(delayMillis: Int) {
//        hideHandler.removeCallbacks(hideRunnable)
//        hideHandler.postDelayed(hideRunnable, delayMillis.toLong())
//    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private const val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private const val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private const val UI_ANIMATION_DELAY = 300
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @SuppressLint("SetTextI18n")
    private fun randomlySetGotoBtnIndex() {
        gotoBtnItemIndex = (Math.random() * wheelLeft!!.data.size).toInt()
//        gotoBtn!!.text = "Goto '" + wheelLeft!!.data[gotoBtnItemIndex!!] + "'"
    }

    override fun onItemSelected(picker: WheelPicker, data: Any, position: Int) {
        var text = ""
        when (picker.id) {
            R.id.main_wheel_left -> {
                text = "Left:"
            }
        }
        (activity as ActivityPatientDetailsUI).userData.vitals.pulse = data.toString()
        imageButtonNext?.isActivated = true
        imageButtonNext?.isEnabled = true
        consultButton?.isActivated = true
        consultButton?.isEnabled = true
        imageButtonNext?.setImageDrawable(((context?.let {
            androidx.core.content.ContextCompat.getDrawable(
                it,
                R.drawable.icon_next_enable
            )
        })))
        Log.i(TAG, "Pulse: ${(activity as ActivityPatientDetailsUI).userData.vitals.pulse}")
//        Toast.makeText(_binding!!.root.context, text + data, Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View?) {
        wheelLeft?.selectedItemPosition = 10
//        randomlySetGotoBtnIndex()
    }

    private fun callNextFragment() {
        if (!(activity as ActivityPatientDetailsUI).dataEditingComplete) {
            val fragment: FragmentBloodPressureScreen = FragmentBloodPressureScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.tempFragment, fragment, pulseScreenTag)
            fragmentTransaction.addToBackStack(pulseScreenTag)
            fragmentTransaction.commit()
        } else {
            (activity as ActivityPatientDetailsUI).dataEditingComplete = false
            val fragment: FragmentFullDetailsScreen = FragmentFullDetailsScreen()
            val fragmentManager = requireActivity().supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.tempFragment, fragment, "FullDetailsReplace")
            fragmentTransaction.addToBackStack("FullDetailsReplace")
            fragmentTransaction.commit()
        }
    }

    private fun callPreviousFragment() {
        (activity as ActivityPatientDetailsUI).onBackPressed()

//        val fragment: WeightScreen = WeightScreen()
//        val fragmentManager = requireActivity().supportFragmentManager
//        val fragmentTransaction = fragmentManager.beginTransaction()
//        fragmentTransaction.add(R.id.tempFragment, fragment)
//        fragmentTransaction.addToBackStack(null)
//        fragmentTransaction.commit()
    }

    //for fragments
    private fun establishVideoConference() {
        val intent: Intent =
            Intent(this.activity as ActivityPatientDetailsUI, ActivityVideoCall::class.java)

//        val blood_pressure: BloodPressure = BloodPressure("80", "120")
//        val personal_details: PersonalDetails = PersonalDetails("vivek_trial", "26", "MALE")
//        val vitals: Vitals = Vitals(blood_pressure, "70", "72", "100", "98")

        val getSessionBodyParam: GetSessionBodyParam =
            GetSessionBodyParam(
                (activity as ActivityPatientDetailsUI).userData.personal_details,
                (activity as ActivityPatientDetailsUI).userData.vitals,
                (activity as ActivityPatientDetailsUI).userData.doctorName
            )
        val bundle = Bundle()
        bundle.putSerializable("data", getSessionBodyParam)
        bundle.putSerializable(
            "doctorName",
            (activity as ActivityPatientDetailsUI).userData.doctorName
        )
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        intent.putExtras(bundle)

        //start video calling activity
        startActivity(intent)

        //pop back all the fragments
        Handler(Looper.getMainLooper()).postDelayed({
            //to re-initiate values
            setInitialValues()
            for (i in 0..((activity as ActivityPatientDetailsUI).supportFragmentManager.backStackEntryCount - 2)) {
                (activity as ActivityPatientDetailsUI).supportFragmentManager.popBackStack()
            }
        }, 500)
    }

    private fun setInitialValues() {
        (activity as ActivityPatientDetailsUI).dataEditingComplete = false
        //Resetting userData of ActivityPatientDetailsUI
        (activity as ActivityPatientDetailsUI).userData = UserData("")
        (activity as ActivityPatientDetailsUI).userData.bloodPressure = BloodPressure("", "")
        (activity as ActivityPatientDetailsUI).userData.vitals =
            Vitals((activity as ActivityPatientDetailsUI).userData.bloodPressure, "", "", "")
        (activity as ActivityPatientDetailsUI).userData.personal_details =
            PersonalDetails("", "", "")
        (activity as ActivityPatientDetailsUI).userData.getSessionBodyParam = GetSessionBodyParam(
            (activity as ActivityPatientDetailsUI).userData.personal_details,
            (activity as ActivityPatientDetailsUI).userData.vitals,
            (activity as ActivityPatientDetailsUI).userData.doctorName
        )
    }
}