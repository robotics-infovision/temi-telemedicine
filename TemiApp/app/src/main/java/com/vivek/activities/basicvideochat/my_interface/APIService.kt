package com.vivek.activities.basicvideochat.my_interface

import com.vivek.activities.basicvideochat.model.ServerData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface APIService {
    @get:GET("session")
    val session: Call<ServerData?>?

    /**
     * URL MANIPULATION
     * HTTP request body with the @Body annotation
     */
    @POST("createSession?id=shubham")
    fun createData(): Call<ServerData?>?

    @POST("createSession?")
    fun getData(@Query("id") doctor: String?): Call<ServerData?>?
}