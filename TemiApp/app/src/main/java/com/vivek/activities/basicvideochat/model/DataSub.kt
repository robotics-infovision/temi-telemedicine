package com.vivek.activities.basicvideochat.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated

@Generated("jsonschema2pojo")
class DataSub : Serializable {
    @SerializedName("sessionId")
    @Expose
    var sessionId: String? = null

    @SerializedName("apiKey")
    @Expose
    var apiKey: String? = null

    @SerializedName("token")
    @Expose
    var token: String? = null

    companion object {
        private const val serialVersionUID = 8954904944998516453L
    }
}