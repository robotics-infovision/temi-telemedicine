package com.vivek.activities.basicvideochat.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import com.vivek.activities.basicvideochat.model.EmailRequest
import com.vivek.activities.basicvideochat.model.EmailResponse
import com.vivek.activities.basicvideochat.repository.EmailViewModelRepo
import com.vivek.activities.basicvideochat.utils.Resource

class EmailViewModel(application: Application) : AndroidViewModel(application)  {
    private val uIMainActivityEmailRepo: EmailViewModelRepo = EmailViewModelRepo()
    val emailRepoLiveData: MediatorLiveData<Resource<EmailResponse>> =
        MediatorLiveData<Resource<EmailResponse>>()

    init {
        emailRepoLiveData.addSource(uIMainActivityEmailRepo.liveDataEmail){
            emailRepoLiveData.value=it
        }
    }

    private val TAG = "EmailViewModel"
    //TODO GetsessionMethod: will call actual method to call api from repository
    fun sendPatientEmail(requestBody: EmailRequest) {
        Log.d(TAG,"inside sendPatientEmail")
        uIMainActivityEmailRepo.sendPatientEmail(requestBody)
    }
}