package com.vivek.activities.basicvideochat.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import com.ebanx.swipebtn.SwipeButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.robotemi.sdk.Robot
import com.robotemi.sdk.TtsRequest
import com.robotemi.sdk.listeners.OnConversationStatusChangedListener
import com.robotemi.sdk.listeners.OnDetectionDataChangedListener
import com.robotemi.sdk.listeners.OnDetectionStateChangedListener
import com.robotemi.sdk.listeners.OnUserInteractionChangedListener
import com.robotemi.sdk.model.DetectionData
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.model.BloodPressure
import com.vivek.activities.basicvideochat.model.GetSessionBodyParam
import com.vivek.activities.basicvideochat.model.PersonalDetails
import com.vivek.activities.basicvideochat.model.Vitals
import com.vivek.activities.basicvideochat.utils.Constants


class UIMainActivity : AppCompatActivity(),
    OnDetectionStateChangedListener,
    OnUserInteractionChangedListener,
    OnDetectionDataChangedListener,
    OnConversationStatusChangedListener,
    Robot.AsrListener,
    Robot.TtsListener,
    AdapterView.OnItemSelectedListener {

//    var viewModel = ViewModelProvider(this@UIMainActivity).get(UIMainActivityViewModel::class.java)

    private var itemselect: Int = 0
    var robot: Robot? = null
    var button: SwipeButton? = null
    var gender = arrayOf("Male", "Female", "Transgender", "Deny")
    lateinit var spinner: Spinner

    //User Vitals
    var edtBPUpper: TextInputEditText? = null
    var edtBPLower: TextInputEditText? = null
    var edtTemp: TextInputEditText? = null
    var edtWeight: TextInputEditText? = null
    var edtPulse: TextInputEditText? = null
    var edtOxygen: TextInputEditText? = null
    var edtAge: TextInputEditText? = null
    var edtGender: String? = "GENDER"
    var edtName: TextInputEditText? = null
    var numberBPUpper: Int = -1
    var numberBPLower: Int = -1
    var numberTemp: Int = -1
    var numberWeight: Int = -1
    var numberPulse: Int = -1
    var numberOxygen: Int = -1
    var numberAge: Int = -1
    var textInputWatcherBPUpper: TextInputLayout? = null
    var textInputWatcherBPLower: TextInputLayout? = null
    var textInputWatcherTemp: TextInputLayout? = null
    var textInputWatcherWeight: TextInputLayout? = null
    var textInputWatcherPulse: TextInputLayout? = null
    var textInputWatcherOxygen: TextInputLayout? = null
    var textInputWatcherAge: TextInputLayout? = null
    var textInputWatcherButton: TextInputLayout? = null

    private var ttsCutCall: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.trial_temi_telemedicine)
        setupUI(findViewById(R.id.UIMainActivity))

        spinner = findViewById(R.id.planets_spinner)
        ArrayAdapter.createFromResource(
            this,
            R.array.gender,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.gender_dropdown)
            spinner.adapter = adapter
        }
        spinner.setOnItemSelectedListener(this)
        spinner.getItemAtPosition(0)

        //vitals Edittext
        edtBPUpper = findViewById(R.id.edtTextBloodPressureUpper)
        edtBPLower = findViewById(R.id.edtTextBloodPressureLower)
        edtTemp = findViewById(R.id.edtTextTemperature)
        edtWeight = findViewById(R.id.edtTextWeight)
        edtPulse = findViewById(R.id.edtTextPulse)
        edtOxygen = findViewById(R.id.edtTextOxygenLevel)
        edtAge = findViewById(R.id.edtTextAge)
        edtName = findViewById(R.id.edtTextName)

        //vitals textwatcher for error setting
        textInputWatcherBPUpper = findViewById(R.id.text_input_ubp)
        textInputWatcherBPLower = findViewById(R.id.text_input_lbp)
        textInputWatcherTemp = findViewById(R.id.text_input_temp)
        textInputWatcherWeight = findViewById(R.id.text_input_weight)
        textInputWatcherPulse = findViewById(R.id.text_input_pulse)
        textInputWatcherOxygen = findViewById(R.id.text_input_spo2)
        textInputWatcherAge = findViewById(R.id.text_input_age)
        textInputWatcherButton = findViewById(R.id.text_input_button)
        this.setTextWatcherProperties()

        //vitals edittext adding listeners
        edtBPUpper?.addTextChangedListener(vitalsText)
        edtBPLower?.addTextChangedListener(vitalsText)
        edtTemp?.addTextChangedListener(vitalsText)
        edtWeight?.addTextChangedListener(vitalsText)
        edtPulse?.addTextChangedListener(vitalsText)
        edtOxygen?.addTextChangedListener(vitalsText)
        edtAge?.addTextChangedListener(vitalsText)
        edtName?.addTextChangedListener(vitalsText)

        //button to call doctor
        button = findViewById(R.id.btn_call_doctor)
        button?.isActivated = false
        button?.isEnabled = false

        //robot functions
        robot = Robot.getInstance()
        robot?.setKioskModeOn(false)

        button?.setOnStateChangeListener {

            if (button?.isActivated == true && button?.isEnabled == true) {
                button?.isEnabled = false
                button?.isActivated = false
                textInputWatcherButton?.isErrorEnabled = false

                //making personalDetails object
                val personal_details = PersonalDetails(
                    edtName?.text.toString().trim(),
                    edtAge?.text.toString().trim(),
                    edtGender.toString()
                )
                Log.i("details", personal_details.toString())

                //making BloodPressure object
                val blood_pressure =
                    BloodPressure(edtBPUpper?.text.toString(), edtBPLower?.text.toString())

                //making Vitals object
                val vitals = Vitals(
                    blood_pressure,
                    edtWeight?.text.toString().trim(),
                    edtPulse?.text.toString().trim(),
                    edtOxygen?.text.toString().trim(),
                    edtTemp?.text.toString().trim()
                )

                //sending data using bungle and intent
                val bodyParam = GetSessionBodyParam(personal_details, vitals, "")
                val intent = Intent(this@UIMainActivity, ActivityVideoCall::class.java)
                val bundle = Bundle()
                bundle.putSerializable("data", bodyParam)

                //initialize variable to null again
                edtBPUpper?.text = null
                edtBPLower?.text = null
                edtTemp?.text = null
                edtWeight?.text = null
                edtPulse?.text = null
                edtOxygen?.text = null
                edtAge?.text = null
                edtName?.text = null
                spinner.setSelection(0)

                intent.putExtras(bundle)
                startActivity(intent)
            } else {
                textInputWatcherButton?.isErrorEnabled = true
                button?.isEnabled = false
                button?.isActivated = false
            }
        }
    }

    fun isInRange(a: Int, b: Int, c: Int, edtText: EditText) {
        if (b > a && c in a..b) {
            return
        } else {
            edtText.text = null
        }
    }

    private val vitalsText: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            //turning data to integer
            setNumbersQuantities()

            //getting gender value
            edtGender = spinner.selectedItem as String?

            //radio button for gender male female other
            //button will be enabled after all fields are filled
            button?.isEnabled = edtBPUpper?.text!!.isNotEmpty() &&
                    edtBPLower?.text!!.isNotEmpty() &&
                    edtAge?.text!!.isNotEmpty() &&
                    edtOxygen?.text!!.isNotEmpty() &&
                    edtTemp?.text!!.isNotEmpty() &&
                    edtWeight?.text!!.isNotEmpty() &&
                    edtPulse?.text!!.isNotEmpty() &&
                    edtName?.text!!.isNotEmpty() &&
                    edtGender != "GENDER"
            button?.isActivated = button?.isEnabled!!

            //applying conditions on user input
            if (numberAge != -1) {
                if (numberAge < Constants.age_lower) {
                    textInputWatcherAge?.isErrorEnabled = true
                    textInputWatcherAge?.error = "Enter field above ${Constants.age_lower}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else if (numberAge > Constants.age_higher) {
                    textInputWatcherAge?.isErrorEnabled = true
                    textInputWatcherAge?.error = "Enter field below ${Constants.age_higher}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else {
                    textInputWatcherAge?.isErrorEnabled = false
                }
            }

            if (numberOxygen != -1) {
                if (numberOxygen < Constants.spo2_lower) {
                    textInputWatcherOxygen?.isErrorEnabled = true
                    textInputWatcherOxygen?.error =
                        "Enter field above ${Constants.spo2_lower}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else if (numberOxygen > Constants.spo2_higher) {
                    textInputWatcherOxygen?.isErrorEnabled = true
                    textInputWatcherOxygen?.error =
                        "Enter field below ${Constants.spo2_higher}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else {
                    textInputWatcherOxygen?.isErrorEnabled = false
                }
            }

            if (numberPulse != -1) {
                if (numberPulse < Constants.pulse_lower) {
                    textInputWatcherPulse?.isErrorEnabled = true
                    textInputWatcherPulse?.error =
                        "Enter field above ${Constants.pulse_lower}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else if (numberPulse > Constants.pulse_higher) {
                    textInputWatcherPulse?.isErrorEnabled = true
                    textInputWatcherPulse?.error =
                        "Enter field below ${Constants.pulse_higher}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else {
                    textInputWatcherPulse?.isErrorEnabled = false
                }
            }

            if (numberTemp != -1) {
                if (numberTemp < Constants.temperature_lower) {
                    textInputWatcherTemp?.isErrorEnabled = true
                    textInputWatcherTemp?.error =
                        "Enter field above ${Constants.temperature_lower}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else if (numberTemp > Constants.temperature_higher) {
                    textInputWatcherTemp?.isErrorEnabled = true
                    textInputWatcherTemp?.error =
                        "Enter field below ${Constants.temperature_higher}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else {
                    textInputWatcherTemp?.isErrorEnabled = false
                }
            }

            if (numberWeight != -1) {
                if (numberWeight < Constants.weight_lower
                ) {
                    textInputWatcherWeight?.isErrorEnabled = true
                    textInputWatcherWeight?.error =
                        "Enter field above ${Constants.weight_lower}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else if (numberWeight > Constants.weight_higher
                ) {
                    textInputWatcherWeight?.isErrorEnabled = true
                    textInputWatcherWeight?.error =
                        "Enter field below ${Constants.weight_higher}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else {
                    textInputWatcherWeight?.isErrorEnabled = false
                }
            }

            if (numberBPUpper != -1) {
                if (numberBPUpper > Constants.blood_pressure_higher) {
                    textInputWatcherBPUpper?.isErrorEnabled = true
                    textInputWatcherBPUpper?.error =
                        "Enter field below ${Constants.blood_pressure_higher}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else {
                    textInputWatcherBPUpper?.isErrorEnabled = false
                }
            }

            if (numberBPLower != -1) {
                if (numberBPLower < Constants.blood_pressure_lower) {
                    textInputWatcherBPLower?.isErrorEnabled = true
                    textInputWatcherBPLower?.error =
                        "Enter field above ${Constants.blood_pressure_lower}"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else {
                    textInputWatcherBPLower?.isErrorEnabled = false
                }
            }

            if (numberBPLower != -1 && numberBPUpper != -1) {
                if (numberBPUpper < numberBPLower) {
                    textInputWatcherBPUpper?.isErrorEnabled = true
                    textInputWatcherBPUpper?.error =
                        "Lower Blood Pressure cannot be higher than Higher Blood Pressure"
                    textInputWatcherBPLower?.isErrorEnabled = true
                    textInputWatcherBPLower?.error =
                        "Higher Blood Pressure cannot be lower than Lower Blood Pressure"
                    button?.isEnabled = false
                    button?.isActivated = button?.isEnabled!!
                } else {
                    textInputWatcherBPUpper?.isErrorEnabled = false
                    textInputWatcherBPLower?.isErrorEnabled = false
                }
            }

            //changing error and hint on button
            if (textInputWatcherBPLower?.isErrorEnabled == false &&
                textInputWatcherBPUpper?.isErrorEnabled == false &&
                textInputWatcherTemp?.isErrorEnabled == false &&
                textInputWatcherOxygen?.isErrorEnabled == false &&
                textInputWatcherPulse?.isErrorEnabled == false &&
                textInputWatcherWeight?.isErrorEnabled == false &&
                textInputWatcherAge?.isErrorEnabled == false &&
                edtGender != "GENDER"
            ) {
                textInputWatcherButton?.error = null
            }
        }

        override fun afterTextChanged(s: Editable) {
        }
    }

    //setting textwatcher properties
    private fun setTextWatcherProperties() {

        textInputWatcherBPUpper?.isHelperTextEnabled = true
        textInputWatcherBPLower?.isHelperTextEnabled = true
        textInputWatcherTemp?.isHelperTextEnabled = true
        textInputWatcherWeight?.isHelperTextEnabled = true
        textInputWatcherPulse?.isHelperTextEnabled = true
        textInputWatcherOxygen?.isHelperTextEnabled = true
        textInputWatcherAge?.isHelperTextEnabled = true

        textInputWatcherBPUpper?.helperText = (getString(R.string.helper_upper_BP))
        textInputWatcherBPLower?.helperText = (getString(R.string.helper_lower_BP))
        textInputWatcherTemp?.helperText = (getString(R.string.helper_temp))
        textInputWatcherWeight?.helperText = (getString(R.string.helper_weight))
        textInputWatcherPulse?.helperText = (getString(R.string.helper_pulse))
        textInputWatcherOxygen?.helperText = (getString(R.string.helper_oxygen))
        textInputWatcherAge?.helperText = (getString(R.string.helper_age))
        textInputWatcherButton?.helperText = (getString(R.string.helper_fill_all_details))

        textInputWatcherBPUpper?.isErrorEnabled = false
        textInputWatcherBPLower?.isErrorEnabled = false
        textInputWatcherTemp?.isErrorEnabled = false
        textInputWatcherWeight?.isErrorEnabled = false
        textInputWatcherPulse?.isErrorEnabled = false
        textInputWatcherOxygen?.isErrorEnabled = false
        textInputWatcherAge?.isErrorEnabled = false
        textInputWatcherButton?.isErrorEnabled = true
        textInputWatcherButton?.error = "Please enter all the details."
    }

    //setting user input values
    private fun setNumbersQuantities() {

        //Age number
        if (edtAge?.text!!.toString() == "") {
            numberAge = -1
        } else {
            numberAge = Integer.parseInt(edtAge?.text!!.toString())
        }
        //BPUpper number
        if (edtBPUpper?.text!!.toString() == "") {
            numberBPUpper = -1
        } else {
            numberBPUpper = Integer.parseInt(edtBPUpper?.text!!.toString())
        }
        //BPLower number
        if (edtBPLower?.text!!.toString() == "") {
            numberBPLower = -1
        } else {
            numberBPLower = Integer.parseInt(edtBPLower?.text!!.toString())
        }
        //Oxygen number
        if (edtOxygen?.text!!.toString() == "") {
            numberOxygen = -1
        } else {
            numberOxygen = Integer.parseInt(edtOxygen?.text!!.toString())
        }
        //Temp number
        if (edtTemp?.text!!.toString() == "") {
            numberTemp = -1
        } else {
            numberTemp = Integer.parseInt(edtTemp?.text!!.toString())
        }
        //Weight number
        if (edtWeight?.text!!.toString() == "") {
            numberWeight = -1
        } else {
            numberWeight = Integer.parseInt(edtWeight?.text!!.toString())
        }
        //Pulse number
        if (edtPulse?.text!!.toString() == "") {
            numberPulse = -1
        } else {
            numberPulse = Integer.parseInt(edtPulse?.text!!.toString())
        }
    }

    //hiding keyboard
    private fun hideSoftKeyboard(activity: Activity) {
        robot?.hideTopBar()
        val inputMethodManager: InputMethodManager = activity.getSystemService(
            INPUT_METHOD_SERVICE
        ) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(
                activity.currentFocus!!.windowToken,
                0
            )
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    fun setupUI(view: View) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { _, _ ->
                hideSoftKeyboard(this@UIMainActivity)
                false
            }
            robot?.hideTopBar()
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                setupUI(innerView)
            }
            robot?.hideTopBar()
        }
    }

    override fun onTtsStatusChanged(ttsRequest: TtsRequest) {
        val tag = "onTtsStatusChanged"
        Log.d(tag, "Id = " + ttsRequest.id)
        Log.d(tag, "Speech = " + ttsRequest.speech)
        Log.d(tag, "Status = " + ttsRequest.status)
        if (((ttsCutCall != null) && ttsRequest.id.toString().equals(ttsCutCall, ignoreCase = true)
                    && (ttsRequest.status == TtsRequest.Status.COMPLETED))
        ) {
            robot!!.goTo(getString(R.string.location_0))
            finish()
        }
    }

    public override fun onStart() {
        super.onStart()
    }

    public override fun onStop() {
        super.onStop()
    }

    override fun onDetectionStateChanged(state: Int) {}
    override fun onUserInteraction(isInteracting: Boolean) {}
    override fun onDetectionDataChanged(detectionData: DetectionData) {}
    override fun onAsrResult(asrResult: String) {}
    override fun onConversationStatusChanged(status: Int, s: String) {}

    //getting values for spinner from dropdown
    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        this.edtGender = p0?.getItemAtPosition(p2).toString()

        button?.isEnabled = edtBPUpper?.text!!.isNotEmpty() &&
                edtBPLower?.text!!.isNotEmpty() &&
                edtAge?.text!!.isNotEmpty() &&
                edtOxygen?.text!!.isNotEmpty() &&
                edtTemp?.text!!.isNotEmpty() &&
                edtWeight?.text!!.isNotEmpty() &&
                edtPulse?.text!!.isNotEmpty() &&
                edtGender != "GENDER"
        button?.isActivated = button?.isEnabled!!

        //changing error and hint on button
        if (textInputWatcherBPLower?.isErrorEnabled == false &&
            textInputWatcherBPUpper?.isErrorEnabled == false &&
            textInputWatcherTemp?.isErrorEnabled == false &&
            textInputWatcherOxygen?.isErrorEnabled == false &&
            textInputWatcherPulse?.isErrorEnabled == false &&
            textInputWatcherWeight?.isErrorEnabled == false &&
            textInputWatcherAge?.isErrorEnabled == false &&
            edtGender != "GENDER"
        ) {
            textInputWatcherButton?.error = null
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }
}