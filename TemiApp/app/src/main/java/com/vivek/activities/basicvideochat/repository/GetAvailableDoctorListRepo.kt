package com.vivek.activities.basicvideochat.repository

import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import com.vivek.activities.basicvideochat.model.AvailableDoctorList
import com.vivek.activities.basicvideochat.my_interface.GetNoticeDataService
import com.vivek.activities.basicvideochat.network.RetrofitInstance
import com.vivek.activities.basicvideochat.utils.Resource
import com.vivek.activities.basicvideochat.utils.Status
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create

class GetAvailableDoctorListRepo : AppCompatActivity() {
    val liveData: MutableLiveData<Resource<AvailableDoctorList>> =
        MutableLiveData<Resource<AvailableDoctorList>>()

    val TAG = "GetAvailableDoctorListRepo"
    fun getAvailableDoctors() {
        setObservableStatus(
            Status.LOADING,
            "Loading",
            null
        )
        Log.i(TAG, "inside getSession()")
        /** Create handle for the RetrofitInstance interface */
        val service: GetNoticeDataService? = RetrofitInstance.getRetrofitInstance()?.create()

        /** Call the method with parameter in the interface to get the notice data */
        val call: Call<AvailableDoctorList>? = service?.getDoctorList()
        /**Log the URL called */
        if (call != null) {
            Log.i("server data URL Called", call.request().url.toString() + "")
        }
        call?.enqueue(object : Callback<AvailableDoctorList> {
            override fun onResponse(call: Call<AvailableDoctorList>, response: Response<AvailableDoctorList>) {
                Log.wtf(
                    TAG,
                    response.body()?.msg
                )
                Log.wtf(
                    TAG,
                    response.body()?.data.toString()
                )

                //use live data to transfer the response data to activity via viewmodel
                if (response.isSuccessful) {
                    setObservableStatus(
                        Status.SUCCESS,
                        "message from Success: get session: onResponse: repo",
                        response.body()
                    )
                } else {
                    setObservableStatus(
                        Status.ERROR,
                        "message from Error: get session: onResponse: repo",
                        response.body()
                    )
                }
            }

            override fun onFailure(call: Call<AvailableDoctorList>, t: Throwable) {
                setObservableStatus(
                    Status.ERROR,
                    "message from Error: get session: onFailure: repo",
                    response = null
                )
                Log.i("Something went wrong:" + t.message, Toast.LENGTH_SHORT.toString())
            }
        })
    }

    fun setObservableStatus(status: Status, message: String, response: AvailableDoctorList?) {
        when (status) {
            Status.SUCCESS -> {
                liveData.value = Resource(Status.SUCCESS, response, message)
            }
            Status.ERROR -> {
                liveData.value = Resource(Status.ERROR, response, message)
            }
            Status.LOADING -> {
                liveData.value = Resource(Status.LOADING, response, message)
            }
        }
    }
}