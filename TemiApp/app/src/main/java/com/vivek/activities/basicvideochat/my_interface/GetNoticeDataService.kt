package com.vivek.activities.basicvideochat.my_interface

import com.vivek.activities.basicvideochat.model.*
import retrofit2.Call
import retrofit2.http.*

interface GetNoticeDataService {
    /**
     * URL MANIPULATION
     * HTTP request body with the @Body annotation
     */
    @POST()
    fun createData(@Url url:String,@Body requestBody: GetSessionBodyParam): Call<ServerData>

    @GET("usersList?type=doctor&username=temi")
    fun getDoctorList(): Call<AvailableDoctorList>

    @POST("addEmail")
    fun updateEmail(@Body requestBody: EmailRequest) :Call<EmailResponse>

    @POST("createSession?")
    fun getData(@Query("id") doctor: String?): Call<ServerData?>?
}