package com.vivek.activities.basicvideochat.activity

import android.Manifest
import android.annotation.SuppressLint
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.opentok.android.*
import com.opentok.android.PublisherKit.PublisherListener
import com.opentok.android.Session.SessionListener
import com.opentok.android.SubscriberKit.SubscriberListener
import com.robotemi.sdk.Robot
import com.robotemi.sdk.TtsRequest
import com.vivek.activities.R
import com.vivek.activities.basicvideochat.model.GetSessionBodyParam
import com.vivek.activities.basicvideochat.model.ServerData
import com.vivek.activities.basicvideochat.my_interface.APIService
import com.vivek.activities.basicvideochat.network.OpenTokConfig
import com.vivek.activities.basicvideochat.network.ServerConfig
import com.vivek.activities.basicvideochat.utils.Status
import com.vivek.activities.basicvideochat.viewmodel.VideoMainActivityViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.EasyPermissions.PermissionCallbacks
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*

class ActivityVideoCall : AppCompatActivity(),
    PermissionCallbacks {

    var location = 0

    lateinit var viewModel: VideoMainActivityViewModel

//    lateinit var vitals: Vitals
//    lateinit var location: List<String>

    lateinit var getSessionBodyParam: GetSessionBodyParam
    var robot: Robot? = null
    private var retrofit: Retrofit? = null
    private var apiService: APIService? = null
    private var session: Session? = null
    var publisher: Publisher? = null
    private var subscriber: Subscriber? = null
    private var publisherViewContainer: FrameLayout? = null
    private var subscriberViewContainer: FrameLayout? = null
    var flMainViewContainer: FrameLayout? = null
    private var ChooseButton: FrameLayout? = null
    private var ControlButtons: LinearLayout? = null
    var temiSpeak: FrameLayout? = null
    private var temiGreetTime: TextView? = null
    private val PLAYER_CONTROLS_VISIBILITY_TIMEOUT = 1000
    var controlHandler = Handler()
    var muted = false
    var dataServer: ServerData = ServerData()

    private val publisherListener: PublisherListener? = object : PublisherListener {
        override fun onStreamCreated(publisherKit: PublisherKit, stream: Stream) {
            Log.d(
                VIDEO_TAG,
                "onStreamCreated: Publisher Stream Created. Own stream " + stream.streamId
            )
        }

        override fun onStreamDestroyed(publisherKit: PublisherKit, stream: Stream) {
            Log.d(
                VIDEO_TAG,
                "onStreamDestroyed: Publisher Stream Destroyed. Own stream " + stream.streamId
            )
            location = 16
            Log.d(locationText, location.toString())
            disconnectSession()
        }

        override fun onError(publisherKit: PublisherKit, opentokError: OpentokError) {
            Log.d(
                VIDEO_TAG,
                "onError: publisherListener Error= " + opentokError.message + " in session: " + session!!.sessionId
            )
            location = 17
            Log.d(locationText, location.toString())
            finishWithMessage(getString(R.string.call_disconnected))
            disconnectSession()
        }
    }
    val sessionListener: SessionListener = object : SessionListener {
        override fun onConnected(session: Session) {
            Log.d(VIDEO_TAG, "onConnected: Connected to session: " + session.sessionId)
            publisher = Publisher.Builder(this@ActivityVideoCall).build()
            publisher?.setPublisherListener(publisherListener)
            publisher?.renderer
                ?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL)
            publisherViewContainer?.addView(publisher?.view)
            if (publisher?.view is GLSurfaceView) {
                (publisher?.view as GLSurfaceView).setZOrderOnTop(true)
            }
            session.publish(publisher)
        }

        override fun onDisconnected(session: Session) {
            location = 18
            Log.d(locationText, location.toString())
            Log.d(VIDEO_TAG, "onDisconnected: Disconnected from session: " + session.sessionId)
            disconnectSession()
        }

        override fun onStreamReceived(session: Session, stream: Stream) {
            Log.d(
                VIDEO_TAG,
                "onStreamReceived: New Stream Received " + stream.streamId + " in session: " + session.sessionId
            )
            temiSpeak?.visibility = View.INVISIBLE
            publisherViewContainer?.visibility = View.VISIBLE
            if (subscriber == null) {
                subscriber = Subscriber.Builder(this@ActivityVideoCall, stream).build()
                subscriber?.renderer?.setStyle(
                    BaseVideoRenderer.STYLE_VIDEO_SCALE,
                    BaseVideoRenderer.STYLE_VIDEO_FILL
                )
                subscriber?.setSubscriberListener(subscriberListener)
                session.subscribe(subscriber)
                subscriberViewContainer?.addView(subscriber?.view)
            }
        }

        override fun onStreamDropped(session: Session, stream: Stream) {
            Log.d(
                VIDEO_TAG,
                "onStreamDropped: Stream Dropped: " + stream.streamId + " in session: " + session.sessionId
            )
            location = 19
            Log.d(locationText, location.toString())
            disconnectSession()
        }

        override fun onError(session: Session, opentokError: OpentokError) {
            Log.d(
                VIDEO_TAG,
                "sessionListener onError: Error= " + opentokError.message + " in session: " + session.sessionId
            )
            location = 20
            Log.d(locationText, location.toString())
            finishWithMessage(getString(R.string.call_disconnected)) // coming when call is disconnected
        }
    }
    var subscriberListener: SubscriberListener = object : SubscriberListener {
        override fun onConnected(subscriberKit: SubscriberKit) {
            Log.d(
                VIDEO_TAG,
                "onConnected: Subscriber connected. Stream: " + subscriberKit.stream.streamId
            )
        }

        override fun onDisconnected(subscriberKit: SubscriberKit) {
            Log.d(
                VIDEO_TAG,
                "onDisconnected: Subscriber disconnected. Stream: " + subscriberKit.stream.streamId
            )
            location = 21
            Log.d(locationText, location.toString())
            disconnectSession()
        }

        override fun onError(subscriberKit: SubscriberKit, opentokError: OpentokError) {
            Log.d(
                VIDEO_TAG,
                "onError: subscriberListener Error= " + opentokError.message + " in session: " + session?.sessionId
            )
            location = 22
            Log.d(locationText, location.toString())
            disconnectSession()
            finishWithMessage(getString(R.string.call_disconnected)) // coming when call is disconnected
        }
    }


    private var pausedAudio = false
    fun buttonPressedMute(view: View) {
        val button = view as ImageButton
        val icon: Int
        if (pausedAudio) {
            pausedAudio = false
            icon = R.drawable.ic_unmuted
            button.background =
                ContextCompat.getDrawable(applicationContext, R.drawable.round_green)
        } else {
            pausedAudio = true
            icon = R.drawable.ic_muted
            button.background = ContextCompat.getDrawable(applicationContext, R.drawable.round_grey)
        }
        button.setImageDrawable(ContextCompat.getDrawable(applicationContext, icon))
    }


    private var pausedVideo = true
    fun buttonPressedVideo(view: View) {
        val button = view as ImageButton
        val icon: Int
        if (pausedVideo) {
            pausedVideo = false
            icon = R.drawable.video_cam_on_trial
        } else {
            pausedVideo = true
            icon = R.drawable.video_cam_off_trial
        }
        button.setImageDrawable(ContextCompat.getDrawable(applicationContext, icon))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.video_activity_main)

        viewModel =
            ViewModelProvider(this).get(VideoMainActivityViewModel::class.java)
        robot = Robot.getInstance()
        addFullStop()
        temiGreet()

        //FrameLayout for text of what tem iis speaking until stream is getting connected
        temiSpeak = findViewById(R.id.waitvideoScreenFrameLayout)
        temiSpeak?.visibility = View.VISIBLE
        temiSpeak?.bringToFront()

        val intent = this.intent
        val bundle = intent.extras
        getSessionBodyParam = bundle?.getSerializable("data") as GetSessionBodyParam
        doctorName = bundle.getSerializable("doctoName").toString()
        Log.i("bundle data:", getSessionBodyParam.toString())

        //Temi Speaks the text provided
        val ttsRequest = TtsRequest.create(
            getString(R.string.greet_hello) +
                    temiGreetTime!!.text.toString() + ". " +
                    getString(R.string.temi_speak_line_3) + ". " +
                    getString(R.string.temi_speak_line_4) + ".",
            false, TtsRequest.Language.EN_US
        )
        robot?.speak(ttsRequest)
        robot?.setKioskModeOn(true)
        flMainViewContainer = findViewById(R.id.main_container)
        ChooseButton = findViewById(R.id.toggle_button)
        ControlButtons = findViewById(R.id.controlBottons)
        publisherViewContainer = findViewById(R.id.publisher_container)
        publisherViewContainer?.visibility = View.GONE
        subscriberViewContainer = findViewById(R.id.subscriber_container)
        requestPermissions()
        controlButtonsAction()
        val muteunmute: ImageButton? = findViewById(R.id.btn_mute_call)
        val cutcall: ImageButton? = findViewById(R.id.btn_call_end)

        //To mute the stream
        muteunmute?.setOnClickListener { view: View ->
            buttonPressedMute(view)
            publisher?.publishAudio = muted
            if (muted) {
                Toast.makeText(this@ActivityVideoCall, "You are unmuted", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this@ActivityVideoCall, "You are muted", Toast.LENGTH_SHORT).show()
            }
            muted = !muted
        }

        //To cut the call
        cutcall?.setOnClickListener {
            sessionListener.onDisconnected(session)
            sessionListener.onStreamDropped(session, subscriber!!.stream)
            disconnectSession()
            finishWithMessage(getString(R.string.call_disconnected)) // coming when call is disconnected
        }
        Log.i("Hello " + robot?.isKioskModeOn(), Toast.LENGTH_SHORT.toString())
    }

    private fun temiGreet() {
        //What to greet (good morning etc)
        val c = Calendar.getInstance()
        val timeOfDay = c[Calendar.HOUR_OF_DAY]
        temiGreetTime = findViewById<TextView>(R.id.temi_first_sentence_part_2)
        if (timeOfDay < 12) {
            temiGreetTime?.setText(R.string.greet_good_morning)
        } else if (timeOfDay in 12..15) {
            temiGreetTime?.setText(R.string.greet_good_afternoon)
        } else if (timeOfDay in 16..23) {
            temiGreetTime?.setText(R.string.greet_good_evening)
        }
    }

    @SuppressLint("SetTextI18n")
    fun addFullStop() {
//        val sentence2: TextView? = findViewById(R.id.temi_first_sentence_part_2)
//        sentence2?.text = getString(R.string.temi_speak_line_2) + "."
//        val sentence3: TextView? = findViewById<TextView>(R.id.temi_third_sentence)
//        sentence3?.text = getString(R.string.temi_speak_line_3) + "."
//        val sentence4: TextView? = findViewById<TextView>(R.id.temi_fourth_sentence)
//        sentence4?.text = getString(R.string.temi_speak_line_4) + "."
    }

    @SuppressLint("ClickableViewAccessibility")
    fun controlButtonsAction() {
        flMainViewContainer!!.setOnTouchListener { _: View?, _: MotionEvent? ->
            animate(0)
            ChooseButton!!.animate().translationY(0f)
            controlHandler.removeCallbacks(hidePlayerControls)
            controlHandler.postDelayed(
                hidePlayerControls,
                PLAYER_CONTROLS_VISIBILITY_TIMEOUT.toLong()
            )
            false
        }
    }

    private fun animate(height: Int) {
        for (i in 0 until ControlButtons!!.childCount) {
            val animate = ControlButtons!!.getChildAt(i).animate()
            animate.startDelay = (i * 100).toLong()
            animate.translationY(height.toFloat())
        }
    }

    private val hidePlayerControls = Runnable {
        animate(
            ControlButtons!!.height
        )
    }

    fun disconnectSession() {
        if (session == null) {
            return
        }
        if (subscriber!!.stream != null) {
            session!!.unsubscribe(subscriber)
            subscriber!!.destroy()
        }
        if (publisher != null) {
            publisherViewContainer!!.removeView(publisher!!.view)
            session!!.unpublish(publisher)
            publisher!!.destroy()
            publisher = null
        }
        session?.disconnect()
//        if (session == null) {
//            return
//        }
//        if (subscriber != null && subscriber?.stream != null) {
//            session?.unsubscribe(subscriber)
//            subscriber?.destroy()
//        }
//        if (publisher != null) {
//            publisherViewContainer?.removeView(publisher?.view)
//            session?.unpublish(publisher)
//            publisher?.destroy()
//            publisher = null
//        }
//        session?.disconnect()
        finishWithMessage(getString(R.string.call_disconnected))
    }

    val locationText = "location"
    override fun onPause() {
        location = 1
        Log.d(locationText, location.toString())
        super.onPause()
        if (session != null) {
            location = 100
            Log.d(locationText, location.toString())
            session!!.onPause()
        }
    }

    override fun onResume() {
        location = 2
        Log.d(locationText, location.toString())
        super.onResume()
        if (session != null) {
            location = 101
            Log.d(locationText, location.toString())
            session!!.onResume()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        location = 3
        Log.d(locationText, location.toString())
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        Log.d(
            TAG,
            "onPermissionsGranted:$requestCode: $perms"
        )
        location = 4
        Log.d(locationText, location.toString())
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        finishWithMessage("onPermissionsDenied: $requestCode: $perms")
        location = 5
        Log.d(locationText, location.toString())
    }

    @AfterPermissionGranted(PERMISSIONS_REQUEST_CODE)
    private fun requestPermissions() {
        val perms = arrayOf(
            Manifest.permission.INTERNET,
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
        )
        location = 6
        Log.d(locationText, location.toString())
        if (EasyPermissions.hasPermissions(this, *perms)) {
            location = 7
            Log.d(locationText, location.toString())
            if (ServerConfig.hasChatServerUrl()) {
                location = 8
                Log.d(locationText, location.toString())
                // Custom server URL exists - retrieve session config
                if (!ServerConfig.isValid) {
                    finishWithMessage(getString(R.string.invalid_chat_server_url) + ServerConfig.CHAT_SERVER_URL)
                    return
                }
                initRetrofit()
                getSession()
            } else {
                location = 9
                Log.d(locationText, location.toString())
                // Use hardcoded session config
                if (!OpenTokConfig.isValid) {
                    finishWithMessage(getString(R.string.invalid_opentokconfig) + OpenTokConfig.getDescription())
                    return
                }
                initializeSession(
                    OpenTokConfig.API_KEY,
                    OpenTokConfig.SESSION_ID,
                    OpenTokConfig.TOKEN
                )
            }
        } else {
            location = 10
            Log.d(locationText, location.toString())
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.rationale_video_app),
                PERMISSIONS_REQUEST_CODE,
                *perms
            )
        }
    }

    /* Make a request for session data */
    private fun getSession() {
        location = 11
        Log.d(locationText, location.toString())
        Log.i(TAG, "inside getSession")
        //call createsession method from viewmodel
        viewModel.getSession(getSessionBodyParam)
    }

    private fun initializeSession(apiKey: String, sessionId: String, token: String) {
        Log.i(TAG, getString(R.string.log_api_initialize_session) + apiKey)
        Log.i(TAG, getString(R.string.log_session_id_initialize_session) + sessionId)
        Log.i(TAG, getString(R.string.log_token_initialize_session) + token)
        location = 12
        Log.d(locationText, location.toString())
        /*
        The context used depends on the specific use case, but usually, it is desired for the session to
        live outside of the Activity e.g: live between activities. For a production applications,
        it's convenient to use Application context instead of Activity context.
         */
        session = Session.Builder(this, apiKey, sessionId).build()
        session?.setSessionListener(sessionListener)
        session?.connect(token)
    }

    private fun initRetrofit() {
        location = 13
        Log.d(locationText, location.toString())
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
        retrofit = Retrofit.Builder()
            .baseUrl(ServerConfig.CHAT_SERVER_URL.toString())
            .addConverterFactory(MoshiConverterFactory.create())
            .client(client)
            .build()
        apiService = retrofit!!.create(APIService::class.java)
    }

    private fun finishWithMessage(message: String) {
        Log.e(TAG, message)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        ActivityPatientDetailsUI.callCompleted = true
        ActivityPatientDetailsUI.callCompletedNumber = 1
        finish()
    }

    public override fun onStart() {
        super.onStart()
        location = 14
        Log.d(locationText, location.toString())
        initObservers()
    }

    public override fun onStop() {
        super.onStop()
    }

    private fun initObservers() {
        location = 15
        Log.d(locationText, location.toString())
        viewModel.videoMainActivityRepoLiveData.observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    dataServer.data = it.data?.data
                    dataServer.msg = it.data?.msg

                    OpenTokConfig.API_KEY = it.data?.data?.apiKey.toString()
                    OpenTokConfig.SESSION_ID = it.data?.data?.sessionId.toString()
                    OpenTokConfig.TOKEN = it.data?.data?.token.toString()

                    initializeSession(
                        OpenTokConfig.API_KEY,
                        OpenTokConfig.SESSION_ID,
                        OpenTokConfig.TOKEN
                    )

                    Log.d(TAG, "API KEY: " + it.data?.data?.apiKey)
                    Log.d(TAG, "SESSION ID: " + it.data?.data?.sessionId)
                    Log.d(TAG, "TOKEN: " + it.data?.data?.token)
                    Log.d(TAG, "MESSAGE FROM SERVER: " + it.data?.msg)
                }
                Status.ERROR -> {
                    Log.d(TAG, "Status: ERROR")
                    Toast.makeText(
                        this@ActivityVideoCall,
                        "Error in getting response",
                        Toast.LENGTH_LONG
                    )
                    //finish with message with message please try again
                }
                Status.LOADING -> {
                    Log.d(TAG, "Status: LOADING")
                }
            }
        }
    }

    companion object {
        private val TAG = ActivityVideoCall::class.java.simpleName
        private const val VIDEO_TAG = "VIDEO_TAG"
        private const val PERMISSIONS_REQUEST_CODE = 124
        private var doctorName: String = ""
    }
}
