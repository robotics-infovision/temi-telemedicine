package com.vivek.activities.basicvideochat.repository

import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import com.vivek.activities.basicvideochat.model.EmailRequest
import com.vivek.activities.basicvideochat.model.EmailResponse
import com.vivek.activities.basicvideochat.my_interface.GetNoticeDataService
import com.vivek.activities.basicvideochat.network.RetrofitInstance
import com.vivek.activities.basicvideochat.utils.Resource
import com.vivek.activities.basicvideochat.utils.Status
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create

class EmailViewModelRepo : AppCompatActivity() {
    val liveDataEmail: MutableLiveData<Resource<EmailResponse>> =
        MutableLiveData<Resource<EmailResponse>>()

    val TAG = "EmailViewModelRepo"
    fun sendPatientEmail(requestBody: EmailRequest) {
//        setObservableStatus(
//            Status.LOADING,
//            "Loading",
//            null
//        )
        Log.i(TAG, "inside sendPatientEmail()")
        /** Create handle for the RetrofitInstance interface */
        val service: GetNoticeDataService? = RetrofitInstance.getRetrofitInstance()?.create()

        /** Call the method with parameter in the interface to get the notice data */
        val call: Call<EmailResponse>? = service?.updateEmail(requestBody)
        /**Log the URL called */
        if (call != null) {
            Log.i("server data URL Called", call.request().url.toString() + "")
        }
        call?.enqueue(object : Callback<EmailResponse> {
            override fun onResponse(call: Call<EmailResponse>, response: Response<EmailResponse>) {
                Log.i(
                    TAG,
                    response.body()?.msg.toString()
                )
                Log.i(
                    TAG,
                    response.body()?.data.toString()
                )

                //use live data to transfer the response data to activity via viewmodel
                if (response.isSuccessful) {
                    setObservableStatus(
                        Status.SUCCESS,
                        "message from Success: get session: onResponse: repo",
                        response.body()
                    )
                } else {
                    setObservableStatus(
                        Status.ERROR,
                        "message from Error: get session: onResponse: repo",
                        response.body()
                    )
                }
            }

            override fun onFailure(call: Call<EmailResponse>, t: Throwable) {
                setObservableStatus(
                    Status.ERROR,
                    "message from Error: get session: onFailure: repo",
                    response = null
                )
                Log.i("Something went wrong:" + t.message, Toast.LENGTH_SHORT.toString())
            }
        })
    }

    fun setObservableStatus(status: Status, message: String, response: EmailResponse?) {
        when (status) {
            Status.SUCCESS -> {
                liveDataEmail.value = Resource(Status.SUCCESS, response, message)
            }
            Status.ERROR -> {
                liveDataEmail.value = Resource(Status.ERROR, response, message)
            }
            Status.LOADING -> {
                liveDataEmail.value = Resource(Status.LOADING, response, message)
            }
        }
    }
}