package com.temi.telemedicine.videocall.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated


@Generated("jsonschema2pojo")
class ServerData : Serializable {
    @SerializedName("msg")
    @Expose
    var msg: String? = null

    @SerializedName("data")
    @Expose
    var data: DataSub? = null

    companion object {
        private const val serialVersionUID = 3857840866360098333L
    }
}