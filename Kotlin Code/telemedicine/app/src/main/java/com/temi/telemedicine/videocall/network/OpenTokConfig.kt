package com.temi.telemedicine.videocall.network

import android.text.TextUtils
import com.temi.telemedicine.videocall.activity.VideoMainActivity


object OpenTokConfig {
    /*
    Fill the following variables using your own Project info from the OpenTok dashboard
    https://dashboard.tokbox.com/projects

    Note that this application will ignore credentials in the `OpenTokConfig` file when `CHAT_SERVER_URL` contains a
    valid URL.
    */
    // Replace with a API key
    var API_KEY: String = VideoMainActivity.data.apiKey.toString()

    // Replace with a generated Session ID
    var SESSION_ID: String = VideoMainActivity.data.sessionId.toString()

    // Replace with a generated token (from the dashboard or using an OpenTok server SDK)
    var TOKEN: String = VideoMainActivity.data.token.toString()

    // *** The code below is to validate this configuration file. You do not need to modify it  ***
    val isValid: Boolean
        get() {
            if ((TextUtils.isEmpty(API_KEY)
                        || TextUtils.isEmpty(SESSION_ID)
                        || TextUtils.isEmpty(TOKEN))
            ) {
                return false
            }
            return true
        }

    private val description: String
        get() = ("OpenTokConfig:" + "\n"
                + "API_KEY: " + API_KEY + "\n"
                + "SESSION_ID: " + SESSION_ID + "\n"
                + "TOKEN: " + TOKEN + "\n")

    @JvmName("getDescription1")
    fun getDescription(): String {
        return this.getDescription()
    }
}