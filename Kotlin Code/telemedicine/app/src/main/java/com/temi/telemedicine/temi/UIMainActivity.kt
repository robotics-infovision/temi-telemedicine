package com.temi.telemedicine.temi

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.robotemi.sdk.Robot
import com.robotemi.sdk.listeners.OnConversationStatusChangedListener
import com.robotemi.sdk.listeners.OnDetectionDataChangedListener
import com.robotemi.sdk.listeners.OnDetectionStateChangedListener
import com.robotemi.sdk.listeners.OnUserInteractionChangedListener
import com.robotemi.sdk.model.DetectionData
import com.temi.telemedicine.R
import com.temi.telemedicine.videocall.activity.VideoMainActivity

class UIMainActivity : AppCompatActivity(),
    OnDetectionStateChangedListener,
    OnUserInteractionChangedListener,
    OnDetectionDataChangedListener,
    OnConversationStatusChangedListener,
    Robot.AsrListener
    {
        var robot: Robot? = null

        var button: Button? = null
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.ui_main_activity)
            button = findViewById<Button>(R.id.btn_call_doctor)

            //robot functions
            robot = Robot.getInstance()
            robot!!.setKioskModeOn(false)
            button!!.setOnClickListener(View.OnClickListener {
                val intent = Intent(this, VideoMainActivity::class.java)
                startActivity(intent)
            })
        }

        override fun onStart() {
            super.onStart()
        }

        override fun onStop() {
            super.onStop()
        }

        override fun onAsrResult(asrResult: String) {
            TODO("Not yet implemented")
        }

        override fun onConversationStatusChanged(status: Int, text: String) {
            TODO("Not yet implemented")
        }

        override fun onDetectionDataChanged(detectionData: DetectionData) {
            TODO("Not yet implemented")
        }

        override fun onDetectionStateChanged(state: Int) {
            TODO("Not yet implemented")
        }

        override fun onUserInteraction(isInteracting: Boolean) {
            TODO("Not yet implemented")
        }
    }